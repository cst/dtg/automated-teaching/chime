CREATE TABLE users
(
    userid        int                    not null primary key,
    username      character varying(255) NOT NULL,
    -- 0 = student
    -- 1 = admin
    -- 2 = viewer
    -- 3 = delegator (some who can grant permission to others to see what they can see)
    role          int                    not null,
    fullname      character varying(255) NOT NULL,
    researchoptin varchar(255)           NOT NULL,
    unique (username)
);

create table sshkeys
(
    keyid  int  not null primary key,
    userid int  not null references users (userid) on delete cascade,
    key    text not null
);

create table repos
(
    userid        int                      not null,
    repoid        int                      not null primary key,
    /* this will usually be the same as the username unless the username has been changed for a user */
    ownerName     varchar(255)             not null,
    repoName      varchar(255)             not null,
    potteryTaskId varchar(255)             not null,
    variant       varchar(255)             not null,
    potteryRepoId varchar(255)             not null,
    started       timestamp with time zone not null,
    unique (potteryRepoId),
    unique (ownerName, repoName)
);

create table ticks
(
    repoid     int                      not null primary key references repos (repoid) on delete cascade,
    deadline   timestamp with time zone not null,
    tickState  varchar(255)             not null,
    assessedBy varchar(255)             not null,
    sha1       varchar(255)             not null,
    online     boolean default 'false'  not null
);

create table submissions
(
    submissionid   int                      not null primary key,
    repoid         int                      not null references repos (repoid) on delete cascade,
    sha1           character varying(255)   not null,
    status         character varying(255)   not null,
    submissionTime timestamp with time zone not null,
    lastUpdateTime timestamp with time zone not null,
    action         varchar(255)             not null,
    errorMessage   text                     not null,
    unique (repoid, sha1)
);

create table steps
(
    submissionid int                    not null references submissions (submissionid) on delete cascade,
    name         character varying(255) not null,
    status       character varying(255) not null,
    output       text                   not null,
    runtimeMs    int                    not null,
    ordinal      int                    not null,
    primary key (submissionid, name)
);

create table assessedexercise
(
    potterytaskid varchar(255)             not null primary key,
    deadline      timestamp with time zone not null,
    online        boolean default 'false'  not null
);

create table leaderboard
(
    userid        int                      not null references users (userid) on delete cascade,
    ordering      int                      not null,
    timerecorded  timestamp with time zone not null,
    score         varchar(255)             not null,
    potteryTaskId varchar(255)             not null,
    primary key (userid, potteryTaskId)
);

create table config
(
    key   varchar(255) not null primary key,
    value text         not null
);

-- A row in this table says that the viewinguser can see the repos of the vieweduser. Its
-- done by name without foreign key so that this information can be added before people
-- have created accounts.
create table viewers
(
    viewingusername character varying(255) NOT NULL,
    viewedusername  character varying(255) NOT NULL,
    primary key (viewingusername, viewedusername)
);

create sequence userids;

create sequence sshkeyids;

create sequence submissionids;

create sequence repoids;

create sequence courseids;