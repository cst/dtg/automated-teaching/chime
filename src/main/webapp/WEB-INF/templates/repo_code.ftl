<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${repo.taskName()} / Code"
current = "code"
owningUser=repo.localRepo().ownerName()
>

    <#if submission.isPresent()>
        <div class="card mb-2">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        Submitted for testing
                        at ${submission.get().submission().submissionTime()?string["yyyy-MM-dd HH:mm"]}.
                        Status ${submission.get().submission().status()}.
                    </div>
                    <div class="ml-auto">
                        <a class="btn btn-primary"
                           href="${submission.get().submissionUrls().report()}"
                           role="button">
                            <span class="fas fa-cogs fa-fw"></span> ${submission.get().submission().sha1()[0..8]}
                        </a>
                        <a class="btn btn-primary"
                           href="${submission.get().submissionUrls().repoUrls().summary()}"><i
                                    class="fa fa-info-circle"></i> ${submission.get().submissionUrls().repoUrls().fullName()}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </#if>
    <#if head && !submission.isPresent()>
        <div class="card mb-2">
            <div class="card-body">
                <div class="d-flex">
                    <div>
                        Not yet submitted for testing
                    </div>
                    <div class="ml-auto">
                        <form action="${repoUrls.newSubmission()}" method="POST">
                            <#if canModify>
                                <input type="submit" class="btn btn-success"
                                       value="New submission"/>
                            </#if>
                            <a class="btn btn-primary"
                               href="${repoUrls.summary()}"><i
                                        class="fa fa-info-circle"></i> ${repoUrls.fullName()}
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </#if>
    <#list files as file>
        <div class="card mb-2">
            <div class="card-header">
                <button class="btn btn-link collapsed"
                        data-toggle="collapse"
                        data-target="#collapse${file.fileName()?replace('[\\./]','','r')}"
                        aria-expanded="true"
                        aria-controls="collapse${file.fileName()?replace('[\\./]','','r')}">
                    ${file.fileName()}
                </button>
            </div>
            <div class="card-body collapse" id="collapse${file.fileName()?replace('[\\./]','','r')}">
                ${file.contents()?no_esc}
            </div>
        </div>
    </#list>

</@c.page>
