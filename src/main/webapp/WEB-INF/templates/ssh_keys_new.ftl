<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="SSH Keys"
current = "ssh_keys"
owningUser=authenticatedUser.userName()
>
    <form action="${urlPrefix}/page/ssh_keys/new" method="POST">
        <div class="form-group">
            <label for="keyTextArea">SSH Key</label>
            <textarea class="form-control" id="keyTextArea" name="key" rows="3"></textarea>
            <small id="keyHelp" class="form-text text-muted">
                You should only ever share the public part of your key.
                The public part is a single, long, line of text that starts with 'ssh-rsa' or similar.
            </small>
        </div>
        <input type="submit" class="btn btn-primary" value="Add new key"/>
    </form>
</@c.page>
