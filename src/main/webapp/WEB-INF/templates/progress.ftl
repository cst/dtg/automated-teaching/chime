<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Progress"
current = "progress"
owningUser=authenticatedUser.userName()
>

    <#list tables.groups() as group>
        <h5>${group}</h5>

        <table class="table w-auto table-bordered table-hover">
            <thead>
            <tr>
                <th scope="col">Username</th>
                <#list tables.tasks(group) as task>
                    <th scope="col">${task}</th>
                </#list>
            </tr>
            </thead>
            <tbody>
            <#list tables.users(group) as user>
                <tr>
                    <th scope="row"><a href="${commonUrls.usersTasks()}${user}">${user}</a></th>
                    <#list tables.tasks(group) as task>
                        <td class="text-center">
                            <#assign progress = tables.get(group,user,task)!''>
                            <#if progress != ''>
                                <a href="${progress.url()}">
                                    <@submissionStatus/>
                                    <@tickStatus/>
                                </a>
                            </#if>
                        </td>
                    </#list>
                </tr>
            </#list>
            </tbody>
        </table>
    </#list>

    <ul>
        <li>Progress on tasks which are not ticked is shown with a single symbol:
            <i class="fas fa-question"></i> (no submissions made),
            <i class="fas fa-times"></i> (no submissions have passed),
            <i class="fas fa-check"></i> (a passing submission has been made).
        </li>
        <li>
            Tasks which are ticked have a second symbol indicating tick status:
            <i class="fas fa-bed"></i> (no assessment has taken place yet),
            <i class="fas fa-frown"></i> (failed to attend),
            <i class="fas fa-bug"></i> (corrections needed),
            <i class="fas fa-check"></i> (tick awarded).
        </li>
        <li>A blank cell means that the user has not started the task in question.</li>
        <li>
            Users who have not started any tasks are not shown in this table.
        </li>
    </ul>

</@c.page>

<#macro submissionStatus>
    <#if progress.submissionStatus().isPresent()>
        <#switch progress.submissionStatus().get()>
            <#case "PENDING">
                <i class="fas fa-bed fa-fw"></i>
                <#break>
            <#case "RUNNING">
                <i class="fas fa-cogs fa-fw"></i>
                <#break>
            <#case "PASSED">
                <i class="fas fa-check fa-fw"></i>
                <#break>
            <#case "FAILED">
                <i class="fas fa-times fa-fw"></i>
                <#break>
            <#default>
                <i class="fas fa-question fa-fw"></i>
        </#switch>
    <#else>
        <i class="fas fa-question fa-fw"></i>
    </#if>
</#macro>

<#macro tickStatus>
    <#if progress.tickStatus().isPresent()>
        <#switch progress.tickStatus().get()>
            <#case "NOT_ASSESSED">
                <i class="fas fa-bed fa-fw mr-2"></i>
                <#break>
            <#case "NO_SHOW">
                <i class="fas fa-frown fa-fw mr-2"></i>
                <#break>
            <#case "CORRECTION_NEEDED">
                <i class="fas fa-bug fa-fw mr-2"></i>
                <#break>
            <#case "AWARDED">
                <i class="fas fa-check fa-fw mr-2"></i>
                <#break>
            <#default>
                <i class="fas fa-question fa-fw"></i>
        </#switch>
    </#if>
</#macro>