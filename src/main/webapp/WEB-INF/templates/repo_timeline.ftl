<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${repo.taskName()} / Timeline"
current = "timeline"
owningUser=repo.localRepo().ownerName()
>

    <div class="card mb-2">
        <div class="card-body">
            <div class="d-flex">
                <div>
                    Timeline
                </div>
                <div class="ml-auto">
                    <a class="btn btn-primary"
                       href="${repoUrls.summary()}"><i
                                class="fa fa-info-circle"></i> ${repoUrls.fullName()}</a>
                </div>
            </div>
        </div>
    </div>

    <#list events as event>
        <div class="card mb-2">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">
                        <span class="btn btn-outline-primary disabled"> ${event.sha1()[0..8]}</span>
                    </div>
                    <div class="col-sm-2">
                        ${event.time()?string["yyyy-MM-dd HH:mm"]}
                    </div>
                    <div class="col-sm-6">
                        ${event.message()}
                    </div>
                    <div class="col-sm-2">
                        <#if event.submissionLink().isPresent()>
                            <a class="btn btn-primary" href="${event.submissionLink().get()}">
                                <span class="fas fa-cogs fa-fw"></span>
                                <span class="sr-only">View submission</span>
                            </a>
                        </#if>
                        <#if event.patchLink().isPresent()>
                            <a class="btn btn-primary" href="${event.patchLink().get()}">
                                <span class="fas fa-paper-plane fa-fw"></span>
                                <span class="sr-only">View patch</span>
                            </a>
                        </#if>
                        <a class="btn btn-primary" href="${event.codeLink()}">
                            <span class="fas fa-code fa-fw"></span>
                            <span class="sr-only">View code</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </#list>
</@c.page>
