<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Programming tasks"
current = "my_tasks"
owningUser=ownerName
>

    <#list repoList as repo>
        <div class="card float-left m-2" style="width: 25rem">
            <div class="card-body">
                <h5 class="card-title" style="width: 22rem; height: 1.25rem; overflow:hidden">${repo.taskName()}</h5>
                <p class="card-text" style="height:10rem">
                    ${repo.problemStatementShort()}
                </p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <#switch repo.overallStatus()>
                        <#case "TEST_IN_PROGRESS">
                            <i class="fas fa-cogs fa-fw"></i> Testing in progress
                            <#break>
                        <#case "PASSED_ALL">
                            <i class="fas fa-check fa-fw"></i> Passed all tests
                            <#break>
                        <#case "LATEST_TEST_FAILED">
                            <i class="fas fa-times fa-fw"></i> Latest test failed
                            <#break>
                        <#case "WORKING">
                            <i class="fas fa-keyboard fa-fw"></i> Work in progress
                            <#break>
                        <#default>
                            <i class="fas fa-question fa-fw"></i> Status unknown
                    </#switch>
                </li>
                <#if repo.tick().isPresent()>
                    <li class="list-group-item">
                        <span class="fas fa-clock fa-fw"></span>
                        Deadline: ${repo.tick().get().deadline()?string["yyyy-MM-dd HH:mm"]}
                    </li>
                </#if>

            </ul>
            <div class="card-body" style="width: 25rem; height: 4rem; overflow:hidden">
                <a href="${repo.repoUrls().summary()}" class="card-link">
                    ${repo.localRepo().ownerName()}/${repo.localRepo().repoName()}
                </a>
            </div>
        </div>
    <#else>
        <div class="card mb-3">
            <div class="card-body">
                Tasks that you have started will be shown here...
            </div>
        </div>
    </#list>
</@c.page>
