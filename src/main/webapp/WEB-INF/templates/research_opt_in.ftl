<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Data sharing consent"
current = "research"
owningUser=authenticatedUser.userName()
>
    <h1 class="display-4">Data sharing consent</h1>

    <p>We wish to conduct a research study into learning and programming using the work you submit to this service.
        Taking part does not change the programming tasks you need to undertake and it does not affect your grade.
        If you choose to take part today, you can withdraw from the study in the future by emailing
        Andrew Rice (acr31) or Alastair Beresford (arb33).</p>

    <p>lf you take part in the study then your work may be included in a dataset of student software submissions shared
        with other researchers. We will replace any occurrences of your CRSID and your registered full name with an
        opaque identifier. This means that a researcher looking at the dataset will be able to group your work together
        but the opaque identifier will not directly identify you as an individual. Please avoid including personal
        details other than your CRSID elsewhere in your program.
    </p>

    <p>I am happy for my work to be included in the study and shared with other researchers:"
    <form action="${urlPrefix}/page/research" method="POST">
        <div class="form-check form-check-inline">
            <input class="form-check-input"
                   type="radio"
                   name="choice"
                   id="inlineRadio1"
                   value="OPT_IN"
                    ${(authenticatedUser.researchOptIn() == "OPT_IN")?then("CHECKED","")}
            >
            <label class="form-check-label" for="inlineRadio1">Yes</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input"
                   type="radio"
                   name="choice"
                   id="inlineRadio2"
                   value="OPT_OUT"
                    ${(authenticatedUser.researchOptIn() == "OPT_OUT")?then("CHECKED","")}
            >
            <label class="form-check-label" for="inlineRadio2">No</label>
        </div>
        <input class="btn btn-danger" type="submit" value="Save">
    </form>

</@c.page>
