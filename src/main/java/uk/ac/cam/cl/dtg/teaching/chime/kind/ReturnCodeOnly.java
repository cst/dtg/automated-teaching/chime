package uk.ac.cam.cl.dtg.teaching.chime.kind;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static uk.ac.cam.cl.dtg.teaching.chime.app.StringUtil.stripNull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;
import java.time.Duration;
import java.util.Date;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.StepDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

public class ReturnCodeOnly implements TaskKind {

  @Override
  public SubmissionDao processSubmission(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      TransactionQueryRunner queryRunner,
      PotteryInterface potteryInterface) {

    ImmutableList<StepDao> steps =
        Streams.mapWithIndex(
                s.getSteps().stream(),
                (step, index) ->
                    StepDao.create(
                        step.getName(),
                        step.getStatus(),
                        stripNull(
                            potteryInterface.getStepOutput(
                                localRepo, s.getTag(), s.getAction(), step.getName())),
                        Duration.ofMillis(step.getMsec()),
                        index))
            .collect(toImmutableList());

    return SubmissionDao.builder()
        .submissionId(submissionId)
        .localRepo(localRepo)
        .sha1(s.getTag())
        .status(interpretStatus(s.getStatus(), s.getErrorMessage()))
        .addAllStep(steps)
        .submissionTime(submissionTime)
        .lastUpdateTime(new Date())
        .errorMessage(stripNull(s.getErrorMessage()))
        .action(s.getAction())
        .build();
  }

  private static String interpretStatus(String taskStatus, String errorMessage) {
    switch (taskStatus) {
      case Submission.STATUS_PENDING:
        return SubmissionDao.STATUS_PENDING;
      case Submission.STATUS_RUNNING:
        return SubmissionDao.STATUS_RUNNING;
      case Submission.STATUS_COMPLETE:
        return errorMessage.isEmpty() ? SubmissionDao.STATUS_PASSED : SubmissionDao.STATUS_FAILED;
    }
    return taskStatus;
  }
}
