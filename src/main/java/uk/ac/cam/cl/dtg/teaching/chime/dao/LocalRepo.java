package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import java.time.Instant;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class LocalRepo {

  public abstract UserDao user();

  public abstract int repoId();

  public abstract String ownerName();

  public abstract String repoName();

  public abstract String potteryTaskId();

  public abstract String variant();

  public abstract String potteryRepoId();

  public abstract Instant startTime();

  public void checkCanBeViewedBy(UserDao requestingUser, TransactionQueryRunner q) {
    user().checkCanBeViewedBy(requestingUser, q);
  }

  public void checkCanBeModifiedBy(UserDao requestingUser, TransactionQueryRunner q) {
    user().checkCanBeModifiedBy(requestingUser, q);
  }

  public static Builder builder() {
    return new AutoValue_LocalRepo.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder user(UserDao user);

    public abstract Builder repoId(int repoId);

    public abstract Builder ownerName(String ownerName);

    public abstract Builder repoName(String repoName);

    public abstract Builder potteryTaskId(String potteryTaskId);

    public abstract Builder variant(String variant);

    public abstract Builder potteryRepoId(String potteryRepoId);

    public abstract Builder startTime(Instant startTime);

    public abstract LocalRepo build();
  }

  public static LocalRepo lookupOrThrow(
      UserDao requestingUser, String ownerName, String repoName, TransactionQueryRunner q) {
    LocalRepo result =
        lookup(ownerName, repoName, q)
            .orElseThrow(
                () ->
                    new DatabaseError("Repository " + ownerName + "/" + repoName + " not found."));
    result.checkCanBeViewedBy(requestingUser, q);
    return result;
  }

  public static Optional<LocalRepo> lookup(
      String ownerName, String repoName, TransactionQueryRunner q) {
    return q.query(
        "SELECT users.userId, "
            + "users.username, "
            + "users.role, "
            + "users.fullname,"
            + "users.researchoptin,"
            + "repos.repoid, "
            + "repos.potteryTaskId, "
            + "repos.variant, "
            + "repos.potteryRepoId,"
            + "repos.started "
            + "from repos, users where "
            + "repos.userId = users.userId and "
            + "ownerName = ? and repoName = ?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          UserDao user =
              UserDao.create(
                  rs.getString("username"),
                  rs.getInt("userid"),
                  rs.getInt("role"),
                  rs.getString("fullname"),
                  UserDao.ResearchChoice.valueOf(rs.getString("researchoptin")));
          return Optional.of(
              builder()
                  .user(user)
                  .repoId(rs.getInt("repoid"))
                  .ownerName(ownerName)
                  .repoName(repoName)
                  .potteryTaskId(rs.getString("potteryTaskId"))
                  .variant(rs.getString("variant"))
                  .potteryRepoId(rs.getString("potteryRepoId"))
                  .startTime(rs.getTimestamp("started").toInstant())
                  .build());
        },
        ownerName,
        repoName);
  }

  public static ImmutableList<LocalRepo> lookup(UserDao user, TransactionQueryRunner q) {
    return q.query(
        "SELECT "
            + "repoId, "
            + "ownerName, "
            + "repoName, "
            + "potteryTaskId, "
            + "variant, "
            + "potteryRepoId,"
            + "started "
            + "from repos where "
            + "userId = ?",
        rs -> {
          ImmutableList.Builder<LocalRepo> builder = ImmutableList.builder();
          while (rs.next()) {
            builder.add(
                builder()
                    .repoId(rs.getInt("repoId"))
                    .user(user)
                    .ownerName(rs.getString("ownerName"))
                    .repoName(rs.getString("repoName"))
                    .potteryTaskId(rs.getString("potteryTaskId"))
                    .variant(rs.getString("variant"))
                    .potteryRepoId(rs.getString("potteryRepoId"))
                    .startTime(rs.getTimestamp("started").toInstant())
                    .build());
          }
          return builder.build();
        },
        user.userId());
  }

  public static ImmutableList<LocalRepo> lookupByTask(
      UserDao user, String taskId, TransactionQueryRunner q) {
    return q.query(
        "SELECT "
            + "repoId, "
            + "ownerName, "
            + "repoName, "
            + "variant, "
            + "potteryRepoId, "
            + "started "
            + "from repos where "
            + "userId = ? and potteryTaskId = ?",
        rs -> {
          ImmutableList.Builder<LocalRepo> builder = ImmutableList.builder();
          while (rs.next()) {
            builder.add(
                builder()
                    .repoId(rs.getInt("repoId"))
                    .user(user)
                    .ownerName(rs.getString("ownerName"))
                    .repoName(rs.getString("repoName"))
                    .potteryTaskId(taskId)
                    .variant(rs.getString("variant"))
                    .potteryRepoId(rs.getString("potteryRepoId"))
                    .startTime(rs.getTimestamp("started").toInstant())
                    .build());
          }
          return builder.build();
        },
        user.userId(),
        taskId);
  }

  public static ImmutableList<LocalRepo> lookupAllByTask(String taskId, TransactionQueryRunner q) {
    return q.query(
        "SELECT "
            + "users.userId, "
            + "users.username, "
            + "users.role, "
            + "users.fullname,"
            + "users.researchoptin,"
            + "repos.repoId, "
            + "repos.ownerName, "
            + "repos.repoName, "
            + "repos.variant, "
            + "repos.potteryRepoId, "
            + "repos.started "
            + "from repos, users where repos.userid = users.userid and "
            + "potteryTaskId = ?",
        rs -> {
          ImmutableList.Builder<LocalRepo> builder = ImmutableList.builder();
          while (rs.next()) {
            UserDao user =
                UserDao.create(
                    rs.getString("username"),
                    rs.getInt("userid"),
                    rs.getInt("role"),
                    rs.getString("fullname"),
                    UserDao.ResearchChoice.valueOf(rs.getString("researchoptin")));
            builder.add(
                builder()
                    .repoId(rs.getInt("repoId"))
                    .user(user)
                    .ownerName(rs.getString("ownerName"))
                    .repoName(rs.getString("repoName"))
                    .potteryTaskId(taskId)
                    .variant(rs.getString("variant"))
                    .potteryRepoId(rs.getString("potteryRepoId"))
                    .startTime(rs.getTimestamp("started").toInstant())
                    .build());
          }
          return builder.build();
        },
        taskId);
  }
}
