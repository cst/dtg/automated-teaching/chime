package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@Produces("text/html")
@Path("/permissions")
public class PermissionsController {

  private final TemplateProcessor templateProcessor;
  private final Database database;
  private final PotteryInterface potteryInterface;
  private final HttpConnectionUri httpConnectionUri;
  private final GitLocation gitLocation;
  private final Messages messages;

  @Inject
  public PermissionsController(
      TemplateProcessor templateProcessor,
      Database database,
      PotteryInterface potteryInterface,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation,
      Messages messages) {
    this.templateProcessor = templateProcessor;
    this.database = database;
    this.potteryInterface = potteryInterface;
    this.httpConnectionUri = httpConnectionUri;
    this.gitLocation = gitLocation;
    this.messages = messages;
  }

  @GET
  @Path("/")
  public String list(@Context UserDao requestingUser) {
    if (!requestingUser.isDelegator() && !requestingUser.isAdmin()) {
      throw new AccessDeniedError("Only users with delegation role can view this page.");
    }

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableSetMultimap<String, UserDao.UserPermission> viewers =
          UserDao.lookupAllViewers(requestingUser, q);
      ImmutableSet<UserDao.UserPermission> viewable =
          UserDao.lookupAllViewerPermissions(requestingUser, q);

      return templateProcessor.process(
          "permissions_list.ftl",
          requestingUser,
          ImmutableMap.of("viewers", viewers, "viewable", viewable));
    }
  }

  @POST
  @Path("/grant")
  public Response grant(
      @Context UserDao requestingUser,
      @FormParam("viewer") String viewer,
      @FormParam("grant") List<String> viewed) {
    if (!requestingUser.isDelegator() && !requestingUser.isAdmin()) {
      throw new AccessDeniedError("Only users with delegation role can view this page.");
    }

    List<String> granted = new ArrayList<>();
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      for (String u : viewed) {
        if (UserDao.grantViewPermission(requestingUser, viewer, u, q)) {
          granted.add(u);
        }
        UserDao.checkViewerRole(viewer, q);
      }
      q.commit();
    }

    if (granted.isEmpty()) {
      messages.postMessage(
          requestingUser,
          "No changes made to permissions for " + viewer + " as suitable access already exists");
    } else {
      messages.postMessage(
          requestingUser, "Granted access to " + viewer + " to view data for " + granted);
    }
    return httpConnectionUri.redirectToFrontend(PermissionsController.class, "");
  }

  @POST
  @Path("/revoke")
  public String revoke(
      @Context UserDao requestingUser,
      @FormParam("viewer") String viewer,
      @FormParam("viewed") List<String> viewed) {
    return templateProcessor.process(
        "permission_revoke_confirm.ftl",
        requestingUser,
        ImmutableMap.of("viewer", viewer, "viewed", viewed));
  }

  @POST
  @Path("/revoke_confirm")
  public Response revokeConfirm(
      @Context UserDao requestingUser,
      @FormParam("viewer") String viewer,
      @FormParam("viewed") List<String> viewed) {
    if (!requestingUser.isDelegator() && !requestingUser.isAdmin()) {
      throw new AccessDeniedError("Only users with delegation role can view this page.");
    }
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      for (String v : viewed) {
        UserDao.revokeViewPermissions(requestingUser, viewer, v, q);
      }
      UserDao.checkViewerRole(viewer, q);
      q.commit();
    }
    messages.postMessage(requestingUser, "Revoked access from " + viewer + " for " + viewed);
    return httpConnectionUri.redirectToFrontend(PermissionsController.class, "");
  }
}
