package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.auto.value.AutoValue;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Table;
import com.google.inject.Inject;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeTask;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskError;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.TickDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@Produces("text/html")
@Path("/progress")
public class ProgressController {
  private static final String ADMINISTRATORS = "Administrators";
  private static final String OTHER = "Other";

  private final Database database;
  private final TemplateProcessor templateProcessor;
  private final PotteryInterface potteryInterface;
  private final HttpConnectionUri httpConnectionUri;

  @Inject
  public ProgressController(
      Database database,
      TemplateProcessor templateProcessor,
      PotteryInterface potteryInterface,
      HttpConnectionUri httpConnectionUri) {
    this.database = database;
    this.templateProcessor = templateProcessor;
    this.potteryInterface = potteryInterface;
    this.httpConnectionUri = httpConnectionUri;
  }

  @GET
  @Path("/export/{taskId}")
  @Produces("text/csv")
  public String export(@Context UserDao userDao, @PathParam("taskId") String taskId) {
    if (!userDao.isAdmin()) {
      throw new AccessDeniedError("Administrators only!");
    }
    if (taskId == null) {
      throw new TaskError("taskId not specified");
    }

    StringBuilder result = new StringBuilder();
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableList<LocalRepo> repos =
          ImmutableList.sortedCopyOf(
              Comparator.comparing(LocalRepo::ownerName), LocalRepo.lookupAllByTask(taskId, q));
      for (LocalRepo repo : repos) {
        Optional<SubmissionDao> latestPassed =
            SubmissionDao.lookupLatestSubmission(repo.repoId(), SubmissionDao.STATUS_PASSED, q);
        String score;
        if (latestPassed.isPresent()) {
          score = "1";
        } else {
          Optional<SubmissionDao> latestSubmission =
              SubmissionDao.lookupLatestSubmission(repo.repoId(), q);
          if (latestSubmission.isPresent()) {
            score = "0.5";
          } else {
            score = "0";
          }
        }
        result
            .append(
                ImmutableList.of(repo.ownerName(), score).stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
            .append("\n");
      }
    }
    return result.toString();
  }

  @GET
  @Path("/")
  public String show(@Context UserDao requestingUser) {

    if (!requestingUser.isAdmin() && !requestingUser.isViewer() && !requestingUser.isDelegator()) {
      throw new AccessDeniedError("Only admins and viewers can see this page");
    }

    Tables tables = new Tables();
    // show a table with all users (sorted by username) against all tasks (sorted by task name)
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableList<UserDao> users = UserDao.lookupAll(requestingUser, q);
      for (UserDao user : users) {

        String group = lookupGroup(q, user);

        ImmutableList<LocalRepo> repos = LocalRepo.lookup(user, q);
        for (LocalRepo repo : repos) {
          ChimeTask taskInfo = potteryInterface.getTaskInfo(repo.potteryTaskId());
          ProgressItem.Builder builder =
              ProgressItem.builder()
                  .fullName(repo.ownerName() + "/" + repo.repoName())
                  .url(
                      httpConnectionUri.url(
                          ChimeRepoController.class, repo.ownerName() + "/" + repo.repoName()));
          TickDao.lookup(repo, q)
              .ifPresent(tick -> builder.tickStatus(tick.tickState().toString()));
          Optional<SubmissionDao> latestPassed =
              SubmissionDao.lookupLatestSubmission(repo.repoId(), SubmissionDao.STATUS_PASSED, q);
          if (latestPassed.isPresent()) {
            builder.submissionStatus(latestPassed.get().status());
          } else {
            SubmissionDao.lookupLatestSubmission(repo.repoId(), q)
                .ifPresent(s -> builder.submissionStatus(s.status()));
          }
          tables.put(group, user.userName(), taskInfo.name(), builder.build());
        }
      }
    }
    return templateProcessor.process(
        "progress.ftl", requestingUser, ImmutableMap.of("tables", tables));
  }

  private String lookupGroup(TransactionQueryRunner q, UserDao user) {
    String group;
    if (user.isAdmin() || user.isViewer() || user.isDelegator()) {
      group = ADMINISTRATORS;
    } else {
      group =
          q.query(
              "SELECT grp from labdb where crsid = ?",
              rs -> {
                if (rs.next()) {
                  return rs.getString(1).replaceAll("\\(.*\\)", "");
                }
                return OTHER;
              },
              user.userName());
    }
    return group;
  }

  public static class Tables {
    private Map<String, Table<String, String, ProgressItem>> tables = new HashMap<>();

    public ImmutableList groups() {
      TreeSet<String> groups = new TreeSet<>(tables.keySet());
      boolean adminPresent = groups.remove(ADMINISTRATORS);
      boolean otherPresent = groups.remove(OTHER);
      ImmutableList.Builder<Object> result = ImmutableList.builder().addAll(groups);
      if (otherPresent) {
        result.add(OTHER);
      }
      if (adminPresent) {
        result.add(ADMINISTRATORS);
      }
      return result.build();
    }

    public ImmutableList users(String group) {
      return ImmutableList.sortedCopyOf(tables.get(group).rowKeySet());
    }

    public ImmutableList tasks(String group) {
      return ImmutableList.sortedCopyOf(tables.get(group).columnKeySet());
    }

    public ProgressItem get(String group, String user, String task) {
      return tables.get(group).get(user, task);
    }

    void put(String group, String user, String task, ProgressItem progressItem) {
      if (!tables.containsKey(group)) {
        tables.put(group, HashBasedTable.create());
      }
      tables.get(group).put(user, task, progressItem);
    }
  }

  @AutoValue
  public abstract static class ProgressItem {

    public abstract String fullName();

    public abstract Optional<String> submissionStatus();

    public abstract Optional<String> tickStatus();

    public abstract String url();

    public static Builder builder() {
      return new AutoValue_ProgressController_ProgressItem.Builder();
    }

    @AutoValue.Builder
    public abstract static class Builder {
      public abstract Builder fullName(String fullName);

      public abstract Builder submissionStatus(String submissionStatus);

      public abstract Builder tickStatus(String tickStatus);

      public abstract Builder url(String url);

      public abstract ProgressItem build();
    }
  }
}
