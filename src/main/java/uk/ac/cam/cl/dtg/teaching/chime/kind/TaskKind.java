package uk.ac.cam.cl.dtg.teaching.chime.kind;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

public interface TaskKind {

  public SubmissionDao processSubmission(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      TransactionQueryRunner queryRunner,
      PotteryInterface potteryInterface);

  static TaskKind loadProcessor(String taskKind) {
    try {
      return (TaskKind)
          Class.forName("uk.ac.cam.cl.dtg.teaching.chime.kind." + taskKind)
              .getConstructor()
              .newInstance();
    } catch (InstantiationException
        | IllegalAccessException
        | InvocationTargetException
        | NoSuchMethodException
        | ClassNotFoundException e) {
      throw new RuntimeException("Failed to load processor: " + taskKind, e);
    }
  }
}
