package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

@AutoValue
public abstract class DiffDto {

  public abstract String originalSha1();

  public abstract String replacementSha1();

  public abstract ImmutableList<DiffFileDto> files();

  public boolean containsDeletion() {
    return files().stream().anyMatch(DiffFileDto::containsDeletion);
  }

  public static Builder builder() {
    return new AutoValue_DiffDto.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder originalSha1(String originalSha1);

    public abstract Builder replacementSha1(String replacementSha1);

    public abstract ImmutableList.Builder<DiffFileDto> filesBuilder();

    public Builder addFile(DiffFileDto file) {
      filesBuilder().add(file);
      return this;
    }

    public abstract DiffDto build();
  }
}
