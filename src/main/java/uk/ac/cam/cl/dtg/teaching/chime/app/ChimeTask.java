package uk.ac.cam.cl.dtg.teaching.chime.app;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.ACTIONS_KEY;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.ADDITIONAL_FILE_FILTER_PREFIX;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.AUTHOR_KEY;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.CRSID_REPLACEMENT_STRING_KEY;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.DEFAULT_CRSID_REPLACEMENT_STRING;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.DEFAULT_TASK_KIND;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.PROBLEM_STATEMENT_KEY_PREFIX;
import static uk.ac.cam.cl.dtg.teaching.chime.app.TaskProperties.TASK_KIND_KEY;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;
import uk.ac.cam.cl.dtg.teaching.pottery.model.TaskInfo;

@AutoValue
public abstract class ChimeTask {

  public abstract String name();

  public abstract String potteryTaskId();

  public abstract String problemStatement();

  public abstract String problemStatementShort();

  public abstract ImmutableList<TaskAction> actions();

  public abstract String kind();

  public abstract String crsidReplacementString();

  public abstract ImmutableSet<String> variants();

  public abstract String authors();

  public ImmutableList<String> actionNames() {
    return actions().stream().map(TaskAction::name).collect(toImmutableList());
  }

  public ImmutableList<TaskAction> followingActions(String actionName) {
    return actions().subList(actionNames().indexOf(actionName) + 1, actions().size());
  }

  public Optional<TaskAction> findNextAction(String actionName) {
    Iterator<TaskAction> step = actions().iterator();
    while (step.hasNext()) {
      TaskAction next = step.next();
      if (next.name().equals(actionName)) {
        return step.hasNext() ? Optional.of(step.next()) : Optional.empty();
      }
    }
    throw new TaskError("Failed to find an action named " + actionName);
  }

  static Builder builder() {
    return new AutoValue_ChimeTask.Builder();
  }

  public static ChimeTask fromTaskInfo(TaskInfo taskInfo) {

    ImmutableList<TaskAction> actions =
        getActionList(taskInfo).stream()
            .map(
                actionName ->
                    TaskAction.create(
                        actionName,
                        getActionStatement(taskInfo, actionName),
                        getAdditionalFileFilter(taskInfo, actionName)))
            .collect(toImmutableList());

    return ChimeTask.builder()
        .name(taskInfo.getName())
        .problemStatement(MarkdownProcessor.toHtml(taskInfo.getProblemStatement()))
        .problemStatementShort(MarkdownProcessor.toShortText(taskInfo.getProblemStatement()))
        .potteryTaskId(taskInfo.getTaskId())
        .actions(actions)
        .kind(taskInfo.getProperties().getOrDefault(TASK_KIND_KEY, DEFAULT_TASK_KIND))
        .crsidReplacementString(
            taskInfo
                .getProperties()
                .getOrDefault(CRSID_REPLACEMENT_STRING_KEY, DEFAULT_CRSID_REPLACEMENT_STRING))
        .authors(taskInfo.getProperties().getOrDefault(AUTHOR_KEY, "The task author"))
        .variants(ImmutableSet.copyOf(taskInfo.getVariants()))
        .build();
  }

  static String getActionStatement(TaskInfo taskInfo, String actionName) {
    return MarkdownProcessor.toHtml(
        taskInfo.getProperties().getOrDefault(PROBLEM_STATEMENT_KEY_PREFIX + actionName, ""));
  }

  static Pattern getAdditionalFileFilter(TaskInfo taskInfo, String actionName) {
    String filter = taskInfo.getProperties().get(ADDITIONAL_FILE_FILTER_PREFIX + actionName);
    if (filter == null) {
      return TaskAction.NEVER_MATCH;
    }
    return Pattern.compile(filter);
  }

  private static List<String> getActionList(TaskInfo taskInfo) {
    return Arrays.asList(
        taskInfo
            .getProperties()
            .getOrDefault(ACTIONS_KEY, Iterables.getFirst(taskInfo.getActions(), null))
            .split("\\s*,\\s*"));
  }

  @AutoValue.Builder
  abstract static class Builder {

    abstract Builder name(String name);

    abstract Builder crsidReplacementString(String crsidReplacementString);

    abstract Builder potteryTaskId(String potteryTaskId);

    abstract Builder problemStatement(String problemStatement);

    abstract Builder problemStatementShort(String problemStatementShort);

    abstract Builder actions(ImmutableList<TaskAction> actions);

    abstract Builder kind(String kind);

    abstract Builder variants(ImmutableSet<String> variants);

    abstract Builder authors(String authors);

    abstract ChimeTask build();
  }
}
