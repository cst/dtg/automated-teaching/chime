package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.base.Suppliers;
import com.google.common.collect.ImmutableList;
import java.util.function.Supplier;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.python.util.PythonInterpreter;

public class Formatter {

  public static final int MAX_CODE_LENGTH = 100000;
  private final Supplier<String> style;
  private final ObjectPool<FormatterWorker> pool;
  public static final boolean DISABLE_FORMATTING = true;

  @Inject
  public Formatter(@Named("pygmentsPythonMaxPoolSize") String poolSizeString) {
    this.style = Suppliers.memoize(Formatter::executeGetStyleDefs);
    GenericObjectPoolConfig<FormatterWorker> config = new GenericObjectPoolConfig<>();
    config.setMaxTotal(Integer.parseInt(poolSizeString));
    this.pool = new GenericObjectPool<>(new FormatterWorkerPoolFactory(), config);
  }

  public String getStyle() {
    return style.get();
  }

  public String format(String code, String filename) {
    if (DISABLE_FORMATTING || code.length() > MAX_CODE_LENGTH) {
      return "<pre>" + code.replace("<", "&lt;").replace(">", "&gt;") + "</pre>";
    }
    try {
      FormatterWorker formatterWorker = pool.borrowObject();
      try {
        return formatterWorker.format(code, filename);
      } finally {
        pool.returnObject(formatterWorker);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private static String executeGetStyleDefs() {
    PythonInterpreter interpreter = new PythonInterpreter();
    interpreter.exec(
        String.join(
            "\n",
            ImmutableList.of(
                "from pygments import highlight",
                "from pygments.lexers import get_lexer_for_filename",
                "from pygments.formatters import HtmlFormatter",
                "result = HtmlFormatter().get_style_defs('.highlight')")));
    return interpreter.get("result", String.class);
  }

  private static class FormatterWorkerPoolFactory extends BasePooledObjectFactory<FormatterWorker> {

    @Override
    public FormatterWorker create() throws Exception {
      PythonInterpreter interpreter = new PythonInterpreter();
      interpreter.exec(
          String.join(
              "\n",
              ImmutableList.of(
                  "from pygments import highlight",
                  "from pygments.lexers import get_lexer_for_filename, get_lexer_for_mimetype",
                  "from pygments.formatters import HtmlFormatter")));
      return new FormatterWorker(interpreter);
    }

    @Override
    public PooledObject<FormatterWorker> wrap(FormatterWorker formatterWorker) {
      return new DefaultPooledObject<>(formatterWorker);
    }
  }

  private static class FormatterWorker {
    private final PythonInterpreter interpreter;

    private FormatterWorker(PythonInterpreter interpreter) {
      this.interpreter = interpreter;
    }

    String format(String code, String filename) {
      interpreter.set("code", code);
      interpreter.set("filename", filename);
      interpreter.exec(
          String.join(
              "\n",
              ImmutableList.of(
                  "try:",
                  "   if filename[-3:] == \".pl\":",
                  "      lexer = get_lexer_for_mimetype(\"text/x-prolog\")",
                  "   else:",
                  "      lexer = get_lexer_for_filename(filename)",
                  "   result = highlight(code, lexer, HtmlFormatter())",
                  "except:",
                  "   result = \"<pre>%s</pre>\" % code")));
      return interpreter.get("result", String.class);
    }
  }
}
