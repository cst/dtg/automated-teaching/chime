package uk.ac.cam.cl.dtg.teaching.chime.database;

import com.mchange.v2.c3p0.DataSources;
import java.io.IOException;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import javax.sql.DataSource;
import org.apache.commons.io.IOUtils;

public class InMemoryDatabase implements Database {

  private static AtomicInteger counter = new AtomicInteger();

  private DataSource dataSource;

  /** Create a new instance (independent of all others). */
  public InMemoryDatabase() {
    try {
      Class.forName("org.hsqldb.jdbcDriver");
    } catch (ClassNotFoundException e) {
      throw new DatabaseError(e);
    }

    try {
      dataSource =
          DataSources.unpooledDataSource("jdbc:hsqldb:mem:" + counter.incrementAndGet(), "SA", "");
      try (TransactionQueryRunner queryRunner = getQueryRunner()) {
        byte[] encoded = IOUtils.toByteArray(Database.class.getResourceAsStream("schema.sql"));
        String shape = new String(encoded);
        shape = Pattern.compile("--.*$", Pattern.MULTILINE).matcher(shape).replaceAll("");
        for (String query : shape.split(";")) {
          query = query.trim();
          if (query.toUpperCase().startsWith("CREATE ")) {
            query = query.replaceAll("text", "character varying(65536)");
            query = query.replaceAll("::integer", "");
            queryRunner.update(query);
          }
        }
        queryRunner.commit();
      }
    } catch (SQLException | IOException e) {
      throw new DatabaseError("Couldn't create testing database", e);
    }
  }

  @Override
  public TransactionQueryRunner getQueryRunner() {
    return new TransactionQueryRunner(dataSource) {
      @Override
      public int nextVal(String sequence) {
        return query(
            "call NEXT VALUE FOR " + sequence,
            rs -> {
              rs.next();
              return rs.getInt(1);
            });
      }
    };
  }

  public interface TestStatement<T> {
    T run(TransactionQueryRunner q) throws Exception;
  }

  public <T> T execute(TestStatement<T> testStatement) {
    try (TransactionQueryRunner q = getQueryRunner()) {
      T result = testStatement.run(q);
      q.commit();
      return result;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void stop() {}
}
