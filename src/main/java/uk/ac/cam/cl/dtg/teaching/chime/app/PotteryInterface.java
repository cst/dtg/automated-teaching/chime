package uk.ac.cam.cl.dtg.teaching.chime.app;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.auto.value.AutoValue;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.pottery.api.PotteryBackend;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RepoExpiredException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RepoNotFoundException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RepoStorageException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RetiredTaskException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.SubmissionNotFoundException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.SubmissionStorageException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.TaskMissingVariantException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.TaskNotFoundException;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.TaskStorageException;
import uk.ac.cam.cl.dtg.teaching.pottery.model.RepoInfoWithStatus;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;
import uk.ac.cam.cl.dtg.teaching.pottery.model.TaskInfo;

public class PotteryInterface {

  private final PotteryBackend potteryBackend;
  private final boolean useTestingVersionOfTasks;
  private final LoadingCache<String, ChimeTask> taskInfoCache =
      CacheBuilder.newBuilder()
          .expireAfterWrite(1, TimeUnit.MINUTES)
          .build(
              new CacheLoader<String, ChimeTask>() {
                @Override
                public ChimeTask load(String key) {
                  try {
                    return ChimeTask.fromTaskInfo(potteryBackend.getTasksController().getTask(key));
                  } catch (TaskNotFoundException | WebApplicationException e) {
                    throw new PotteryError(e);
                  }
                }
              });
  private final LoadingCache<SkeletonFileKey, ByteString> skeletonFileContentsCache =
      CacheBuilder.newBuilder()
          .expireAfterWrite(1, TimeUnit.MINUTES)
          .build(
              new CacheLoader<SkeletonFileKey, ByteString>() {
                @Override
                public ByteString load(SkeletonFileKey key) {
                  try {
                    Response response =
                        potteryBackend
                            .getTasksController()
                            .readSkeletonFile(
                                key.taskId(),
                                key.variant(),
                                "file",
                                key.name(),
                                useTestingVersionOfTasks);
                    try (InputStream is = response.readEntity(InputStream.class)) {
                      return ByteString.readFrom(is);
                    }
                  } catch (TaskNotFoundException
                      | TaskMissingVariantException
                      | WebApplicationException
                      | IOException e) {
                    throw new PotteryError(e);
                  }
                }
              });

  private final LoadingCache<TaskVariantKey, ImmutableList<String>> skeletonFileNameCache =
      CacheBuilder.newBuilder()
          .expireAfterWrite(1, TimeUnit.MINUTES)
          .build(
              new CacheLoader<TaskVariantKey, ImmutableList<String>>() {
                @Override
                public ImmutableList<String> load(TaskVariantKey key) {
                  try {
                    return ImmutableList.copyOf(
                        potteryBackend
                            .getTasksController()
                            .listSkeletonFiles(
                                key.taskId(), key.variant(), useTestingVersionOfTasks));
                  } catch (TaskNotFoundException
                      | TaskMissingVariantException
                      | TaskStorageException
                      | WebApplicationException e) {
                    throw new PotteryError(e);
                  }
                }
              });

  private final LoadingCache<Integer, ImmutableMap<String, String>> statusCache =
      CacheBuilder.newBuilder()
          .expireAfterWrite(5, TimeUnit.SECONDS)
          .build(
              new CacheLoader<Integer, ImmutableMap<String, String>>() {
                @Override
                public ImmutableMap<String, String> load(Integer key) {
                  return ImmutableMap.copyOf(potteryBackend.getStatusController().getStatus());
                }
              });

  @Inject
  public PotteryInterface(
      PotteryBackend potteryBackend,
      @Named("useTestingVersionOfTasks") boolean useTestingVersionOfTasks) {
    this.potteryBackend = potteryBackend;
    this.useTestingVersionOfTasks = useTestingVersionOfTasks;
  }

  public Collection<ChimeTask> listTasks() {
    Collection<TaskInfo> taskInfos =
        useTestingVersionOfTasks
            ? potteryBackend.getTasksController().listTesting()
            : potteryBackend.getTasksController().listRegistered();
    return taskInfos.stream().map(ChimeTask::fromTaskInfo).collect(toImmutableList());
  }

  public ChimeTask getTaskInfo(String taskId) {
    try {
      return taskInfoCache.get(taskId);
    } catch (ExecutionException | UncheckedExecutionException e) {
      throw new PotteryError(e.getCause());
    }
  }

  public ByteString readSkeletonFile(String taskId, String variant, String skeletonFile) {
    try {
      return skeletonFileContentsCache.get(createSkeletonFileKey(taskId, variant, skeletonFile));
    } catch (ExecutionException | UncheckedExecutionException e) {
      throw new PotteryError(e.getCause());
    }
  }

  public ImmutableList<String> getSkeletonFileNames(LocalRepo localRepo) {
    try {
      return skeletonFileNameCache.get(
          createTaskVariantKey(localRepo.potteryTaskId(), localRepo.variant()));
    } catch (ExecutionException | UncheckedExecutionException e) {
      throw new PotteryError(e.getCause());
    }
  }

  public ImmutableMap<String, String> getStatus() {
    try {
      return statusCache.get(0);
    } catch (ExecutionException | UncheckedExecutionException e) {
      throw new PotteryError(e.getCause());
    }
  }

  public Submission getSubmission(LocalRepo localRepo, String sha1, String action) {
    try {
      return potteryBackend
          .getSubmissionsController()
          .getSubmission(localRepo.potteryRepoId(), sha1, action);
    } catch (SubmissionNotFoundException
        | RepoStorageException
        | RepoNotFoundException
        | SubmissionStorageException
        | WebApplicationException e) {
      throw new PotteryError(e);
    }
  }

  public String getStepOutput(LocalRepo localRepo, String headSha, String action, String stepName) {
    try {
      String output =
          potteryBackend
              .getSubmissionsController()
              .getOutput(localRepo.potteryRepoId(), headSha, action, stepName);
      return output == null ? "" : output;
    } catch (SubmissionNotFoundException
        | RepoStorageException
        | RepoNotFoundException
        | SubmissionStorageException
        | WebApplicationException e) {
      throw new PotteryError(e);
    }
  }

  public RepoInfoWithStatus createRemoteRepo(String taskId, String variant, String sshUrl) {
    try {
      return potteryBackend
          .getRepoController()
          .makeRemoteRepo(taskId, useTestingVersionOfTasks, -1, variant, sshUrl, 0);
    } catch (TaskNotFoundException
        | RepoExpiredException
        | RepoStorageException
        | RepoNotFoundException
        | RetiredTaskException
        | TaskMissingVariantException
        | WebApplicationException e) {
      throw new PotteryError(e);
    }
  }

  public Submission scheduleTest(LocalRepo localRepo, String action, String headSha) {
    try {
      return potteryBackend
          .getSubmissionsController()
          .scheduleTest(localRepo.potteryRepoId(), headSha, action);
    } catch (RepoStorageException
        | RepoExpiredException
        | RepoNotFoundException
        | SubmissionStorageException
        | WebApplicationException e) {
      throw new PotteryError(e);
    }
  }

  public String publicKey() {
    try {
      return potteryBackend.getStatusController().publicKey();
    } catch (WebApplicationException e) {
      throw new PotteryError(e);
    }
  }

  @AutoValue
  abstract static class SkeletonFileKey {
    abstract String taskId();

    abstract String variant();

    abstract String name();
  }

  private static SkeletonFileKey createSkeletonFileKey(String taskId, String variant, String name) {
    return new AutoValue_PotteryInterface_SkeletonFileKey(taskId, variant, name);
  }

  @AutoValue
  abstract static class TaskVariantKey {
    abstract String taskId();

    abstract String variant();
  }

  private static TaskVariantKey createTaskVariantKey(String taskId, String variant) {
    return new AutoValue_PotteryInterface_TaskVariantKey(taskId, variant);
  }
}
