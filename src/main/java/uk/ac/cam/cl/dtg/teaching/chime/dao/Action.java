package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.util.Optional;

@AutoValue
public abstract class Action {

  public abstract String name();

  public abstract String description();

  public abstract Optional<SubmissionDao> latestSubmission();

  public abstract Optional<SubmissionDao> latestPassed();

  public static Builder builder() {
    return new AutoValue_Action.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder name(String name);

    public abstract Builder description(String description);

    public abstract Builder latestSubmission(SubmissionDao latestSubmission);

    public abstract Builder latestPassed(SubmissionDao latestPassed);

    public abstract Action build();
  }
}
