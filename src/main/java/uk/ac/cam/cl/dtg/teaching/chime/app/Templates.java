package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.protobuf.ByteString;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import org.apache.commons.io.IOUtils;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;

public class Templates {

  public static String getJavaHeader(ChimeTask task, UserDao user) {
    return loadTemplate("apache-header-java-2.0.txt")
        .replace("<yyyy>", String.valueOf(LocalDate.now().getYear()))
        .replace("[name of copyright owner]", task.authors() + ", " + user.fullName());
  }

  public static ByteString getNotice(ChimeTask task, UserDao user) {
    return ByteString.copyFrom(
        loadTemplate("apache-notice-2.0.txt")
            .replace("[name of task]", task.name())
            .replace("<yyyy>", String.valueOf(LocalDate.now().getYear()))
            .replace("[name of copyright owner]", task.authors() + ", " + user.fullName()),
        StandardCharsets.UTF_8);
  }

  public static ByteString getLicenseFile() {
    return ByteString.copyFrom(loadTemplate("apache-license-2.0.txt"), StandardCharsets.UTF_8);
  }

  private static String loadTemplate(String name) {
    InputStream resource = Templates.class.getResourceAsStream(name);
    if (resource == null) {
      throw new IOError(new IOException("Failed to find resource " + name));
    }
    try {
      return IOUtils.toString(resource, "UTF-8");
    } catch (IOException e) {
      throw new IOError(e);
    }
  }
}
