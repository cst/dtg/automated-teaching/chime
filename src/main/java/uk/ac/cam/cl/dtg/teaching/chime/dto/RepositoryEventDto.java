package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;
import org.eclipse.jgit.revwalk.RevCommit;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeRepoController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeSubmissionsController;

@AutoValue
public abstract class RepositoryEventDto {

  public abstract Date time();

  public abstract String message();

  public abstract String sha1();

  public abstract String codeLink();

  public abstract Optional<String> patchLink();

  public abstract Optional<String> submissionLink();

  public static RepositoryEventDto createForCommit(
      RevCommit commit, LocalRepo localRepo, HttpConnectionUri httpConnectionUri) {
    return RepositoryEventDto.builder()
        .time(Date.from(Instant.ofEpochSecond(commit.getCommitTime())))
        .message(commit.getShortMessage())
        .sha1(commit.toObjectId().getName())
        .codeLink(createCodeLink(commit.toObjectId().getName(), localRepo, httpConnectionUri))
        .build();
  }

  public static RepositoryEventDto createForPatch(
      RevCommit commit,
      RevCommit previousCommit,
      LocalRepo localRepo,
      HttpConnectionUri httpConnectionUri) {
    return RepositoryEventDto.builder()
        .time(Date.from(Instant.ofEpochSecond(commit.getCommitTime())))
        .message(commit.getShortMessage())
        .sha1(commit.toObjectId().getName())
        .codeLink(createCodeLink(commit.toObjectId().getName(), localRepo, httpConnectionUri))
        .patchLink(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format(
                    "%s/%s/diff/%s/%s",
                    localRepo.ownerName(),
                    localRepo.repoName(),
                    previousCommit.toObjectId().getName(),
                    commit.toObjectId().getName())))
        .build();
  }

  public static RepositoryEventDto createForSubmission(
      SubmissionDao submission, LocalRepo localRepo, HttpConnectionUri httpConnectionUri) {
    return RepositoryEventDto.builder()
        .time(submission.submissionTime())
        .message("Submission: " + submission.status())
        .sha1(submission.sha1())
        .codeLink(createCodeLink(submission.sha1(), localRepo, httpConnectionUri))
        .submissionLink(
            httpConnectionUri.url(ChimeSubmissionsController.class, submission.submissionId()))
        .build();
  }

  public static Builder builder() {
    return new AutoValue_RepositoryEventDto.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder time(Date time);

    public abstract Builder message(String message);

    public abstract Builder sha1(String sha1);

    public abstract Builder codeLink(String codeLink);

    public abstract Builder patchLink(String patchLink);

    public abstract Builder submissionLink(String submissionLink);

    public abstract RepositoryEventDto build();
  }

  private static String createCodeLink(
      String commit, LocalRepo localRepo, HttpConnectionUri httpConnectionUri) {
    return httpConnectionUri.url(
        ChimeRepoController.class,
        String.format("%s/%s/code/%s", localRepo.ownerName(), localRepo.repoName(), commit));
  }
}
