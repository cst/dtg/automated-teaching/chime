package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.sql.Timestamp;
import java.time.Instant;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class NewLocalRepo {

  public abstract UserDao user();

  public abstract String ownerName();

  public abstract String repoName();

  public abstract String potteryTaskId();

  public abstract String variant();

  public abstract String potteryRepoId();

  public abstract Instant startTime();

  public static Builder builder() {
    return new AutoValue_NewLocalRepo.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder user(UserDao user);

    public abstract Builder ownerName(String ownerName);

    public abstract Builder repoName(String repoName);

    public abstract Builder potteryTaskId(String potteryTaskId);

    public abstract Builder variant(String variant);

    public abstract Builder potteryRepoId(String potteryRepoId);

    public abstract Builder startTime(Instant startTime);

    public abstract NewLocalRepo build();
  }

  public LocalRepo insert(TransactionQueryRunner q) {

    int newRepoId = q.nextVal("repoids");

    q.insert(
        "INSERT into repos("
            + "userId,"
            + "repoId,"
            + "ownerName,"
            + "repoName,"
            + "potteryTaskId,"
            + "variant,"
            + "potteryRepoId,"
            + "started) "
            + "values (?,?,?,?,?,?,?,?)",
        user().userId(),
        newRepoId,
        ownerName(),
        repoName(),
        potteryTaskId(),
        variant(),
        potteryRepoId(),
        Timestamp.from(startTime()));

    return LocalRepo.builder()
        .user(user())
        .repoId(newRepoId)
        .ownerName(ownerName())
        .repoName(repoName())
        .potteryTaskId(potteryTaskId())
        .variant(variant())
        .potteryRepoId(potteryRepoId())
        .startTime(startTime())
        .build();
  }
}
