package uk.ac.cam.cl.dtg.teaching.chime.urls;

import com.google.auto.value.AutoValue;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeTasksController;

@AutoValue
public abstract class TaskUrls {

  public abstract String startTask();

  public static Builder builder() {
    return new AutoValue_TaskUrls.Builder();
  }

  public static TaskUrls createNew(String potteryTaskId, HttpConnectionUri httpConnectionUri) {
    return TaskUrls.builder()
        .startTask(httpConnectionUri.url(ChimeTasksController.class, potteryTaskId))
        .build();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder startTask(String startTask);

    public abstract TaskUrls build();
  }
}
