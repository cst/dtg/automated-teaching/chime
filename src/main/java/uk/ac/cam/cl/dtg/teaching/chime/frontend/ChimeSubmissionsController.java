package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.io.File;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeFileWriter;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeTask;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitApiError;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.SkeletonFiles;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskAction;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.chime.dto.SubmissionDto;
import uk.ac.cam.cl.dtg.teaching.chime.kind.TaskKind;
import uk.ac.cam.cl.dtg.teaching.chime.urls.RepoUrls;
import uk.ac.cam.cl.dtg.teaching.chime.urls.SubmissionUrls;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

@Produces("text/html")
@Path("/submissions")
public class ChimeSubmissionsController {

  public static final String INITIALISED_NEXT_STEP = "Initialised next step";
  private final Database database;
  private final TemplateProcessor templateProcessor;
  private final PotteryInterface potteryInterface;
  private final HttpConnectionUri httpConnectionUri;
  private final GitLocation gitLocation;

  @Inject
  public ChimeSubmissionsController(
      Database database,
      TemplateProcessor templateProcessor,
      PotteryInterface potteryInterface,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation) {
    this.database = database;
    this.templateProcessor = templateProcessor;
    this.potteryInterface = potteryInterface;
    this.httpConnectionUri = httpConnectionUri;
    this.gitLocation = gitLocation;
  }

  @GET
  @Path("/{submissionId}")
  public String showSubmission(
      @Context UserDao requestingUser, @PathParam("submissionId") Integer submissionId) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      SubmissionDao submissionDao = SubmissionDao.lookupOrThrow(requestingUser, submissionId, q);
      SubmissionDto submissionDto =
          SubmissionDto.builder()
              .submission(submissionDao)
              .submissionUrls(
                  SubmissionUrls.createNew(
                      requestingUser, submissionDao, httpConnectionUri, gitLocation))
              .build();
      ChimeTask taskInfo =
          potteryInterface.getTaskInfo(submissionDto.submission().localRepo().potteryTaskId());

      ImmutableMap<String, String> status = potteryInterface.getStatus();
      int inQueue = Integer.parseInt(status.getOrDefault("Worker.queueSize", "0")) - 1;
      String aheadInQueue = inQueue == -1 ? "?" : String.valueOf(inQueue);
      return templateProcessor.process(
          "submission_show.ftl",
          requestingUser,
          RepoUrls.createNew(
              requestingUser, submissionDao.localRepo(), httpConnectionUri, gitLocation),
          ImmutableMap.of(
              "submission",
              submissionDto,
              "aheadInQueue",
              aheadInQueue,
              "taskName",
              taskInfo.name()));
    }
  }

  @POST
  @Path("/{submissionId}")
  public Response refresh(
      @Context UserDao requestingUser, @PathParam("submissionId") Integer submissionId) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      SubmissionDao submissionDao = SubmissionDao.lookupOrThrow(requestingUser, submissionId, q);
      submissionDao.checkCanBeModifiedBy(requestingUser, q);
      updateSubmission(submissionDao, q, gitLocation, potteryInterface);
      q.commit();
    }
    return httpConnectionUri.redirectToFrontend(
        ChimeSubmissionsController.class, String.valueOf(submissionId));
  }

  public static void updateSubmission(
      SubmissionDao submissionDao,
      TransactionQueryRunner queryRunner,
      GitLocation gitLocation,
      PotteryInterface potteryInterface) {

    ChimeTask taskInfo = potteryInterface.getTaskInfo(submissionDao.localRepo().potteryTaskId());

    TaskKind processor = TaskKind.loadProcessor(taskInfo.kind());

    Submission s =
        potteryInterface.getSubmission(
            submissionDao.localRepo(), submissionDao.sha1(), submissionDao.action());
    SubmissionDao updated =
        processor.processSubmission(
            submissionDao.submissionId(),
            submissionDao.localRepo(),
            submissionDao.submissionTime(),
            s,
            queryRunner,
            potteryInterface);
    updated.update(queryRunner);
    if (updated.status().equals(SubmissionDao.STATUS_PASSED)) {
      taskInfo
          .findNextAction(updated.action())
          .ifPresent(
              nextAction ->
                  updateSkeletonFiles(
                      nextAction, taskInfo, updated, gitLocation, potteryInterface));
    }
  }

  private static void updateSkeletonFiles(
      TaskAction nextAction,
      ChimeTask taskInfo,
      SubmissionDao submissionDao,
      GitLocation gitLocation,
      PotteryInterface potteryInterface) {
    File repoLocation = gitLocation.getDirectory(submissionDao.localRepo());
    try (Git g = Git.open(repoLocation)) {
      g.reset().setMode(ResetCommand.ResetType.HARD).setRef("HEAD").call();
      ChimeFileWriter fileWriter = new ChimeFileWriter(repoLocation, g);
      SkeletonFiles.updateSkeletonFiles(
          submissionDao.localRepo().user(),
          taskInfo,
          nextAction.name(),
          () -> potteryInterface.getSkeletonFileNames(submissionDao.localRepo()),
          skeletonFileName ->
              potteryInterface.readSkeletonFile(
                  taskInfo.potteryTaskId(), submissionDao.localRepo().variant(), skeletonFileName),
          fileWriter);
      if (fileWriter.isWritten()) {
        g.commit().setMessage(INITIALISED_NEXT_STEP).call();
      }
    } catch (IOException | GitAPIException e) {
      throw new GitApiError(e);
    }
  }
}
