package uk.ac.cam.cl.dtg.teaching.chime.urls;

import com.google.auto.value.AutoValue;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeRepoController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeSubmissionsController;

@AutoValue
public abstract class SubmissionUrls {

  public abstract RepoUrls repoUrls();

  public abstract String report();

  public abstract String code();

  public abstract String update();

  public static Builder builder() {
    return new AutoValue_SubmissionUrls.Builder();
  }

  public static SubmissionUrls createNew(
      UserDao requestingUser,
      SubmissionDao submissionDao,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation) {
    return SubmissionUrls.builder()
        .repoUrls(
            RepoUrls.createNew(
                requestingUser, submissionDao.localRepo(), httpConnectionUri, gitLocation))
        .report(
            httpConnectionUri.url(ChimeSubmissionsController.class, submissionDao.submissionId()))
        .code(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format(
                    "%s/%s/code/%s",
                    submissionDao.localRepo().ownerName(),
                    submissionDao.localRepo().repoName(),
                    submissionDao.sha1())))
        .update(
            httpConnectionUri.url(ChimeSubmissionsController.class, submissionDao.submissionId()))
        .build();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder update(String update);

    public abstract Builder report(String report);

    public abstract Builder code(String code);

    public abstract Builder repoUrls(RepoUrls repoUrls);

    public abstract SubmissionUrls build();
  }
}
