package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class UserDao {

  public enum ResearchChoice {
    OPT_IN,
    OPT_OUT,
    UNSET;
  }

  public static final int ROLE_NORMAL = 0;
  public static final int ROLE_ADMIN = 1;
  public static final int ROLE_VIEWER = 2;
  public static final int ROLE_DELEGATOR = 3;

  public abstract String userName();

  public abstract int userId();

  public abstract int role();

  public abstract String fullName();

  public abstract ResearchChoice researchOptIn();

  public boolean needsResearchChoice() {
    return researchOptIn().equals(ResearchChoice.UNSET);
  }

  public boolean isAdmin() {
    return role() == ROLE_ADMIN;
  }

  public boolean isViewer() {
    return role() == ROLE_VIEWER;
  }

  public boolean isDelegator() {
    return role() == ROLE_DELEGATOR;
  }

  public boolean canBeModifiedBy(UserDao requestingUser, TransactionQueryRunner q) {
    if (requestingUser.isAdmin()) {
      return true;
    }
    return userId() == requestingUser.userId();
  }

  public void checkCanBeModifiedBy(UserDao requestingUser, TransactionQueryRunner q) {
    if (canBeModifiedBy(requestingUser, q)) {
      return;
    }
    throw new AccessDeniedError(requestingUser, this);
  }

  public boolean canBeViewedBy(UserDao requestingUser, TransactionQueryRunner q) {
    if (requestingUser.isAdmin()) {
      return true;
    }
    if (userId() == requestingUser.userId()) {
      return true;
    }
    if ((requestingUser.isViewer() || requestingUser.isDelegator())
        && q.query(
            "SELECT * From viewers where viewingusername = ? and viewedusername = ?",
            ResultSet::next,
            requestingUser.userName(),
            userName())) {
      return true;
    }
    return false;
  }

  public void checkCanBeViewedBy(UserDao requestingUser, TransactionQueryRunner q) {
    if (canBeViewedBy(requestingUser, q)) {
      return;
    }
    throw new AccessDeniedError(requestingUser, this);
  }

  public void updateResearchOptIn(ResearchChoice researchChoice, TransactionQueryRunner q) {
    q.update("UPDATE users set researchoptin = ? where userid=?", researchChoice.name(), userId());
  }

  static UserDao create(
      String userName, int userId, int role, String fullName, ResearchChoice researchChoice) {
    checkUserName(userName);
    return new AutoValue_UserDao(userName, userId, role, fullName, researchChoice);
  }

  private static UserDao create(ResultSet rs) throws SQLException {
    return create(
        rs.getString("userName"),
        rs.getInt("userId"),
        rs.getInt("role"),
        rs.getString("fullname"),
        ResearchChoice.valueOf(rs.getString("researchoptin")));
  }

  public static UserDao insert(
      String userName, int role, String fullName, TransactionQueryRunner queryRunner) {
    checkUserName(userName);
    int nextUserId = queryRunner.nextVal("userids");
    queryRunner.update(
        "INSERT into users(userid, userName,role,fullname,researchoptin) values (?,?,?,?,?)",
        nextUserId,
        userName,
        role,
        fullName,
        ResearchChoice.UNSET.name());
    return create(userName, nextUserId, role, fullName, ResearchChoice.UNSET);
  }

  public static Optional<UserDao> lookup(String userName, TransactionQueryRunner queryRunner) {
    return queryRunner.query(
        "SELECT userId,username, role,fullname,researchoptin from users where userName= ?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          return Optional.of(create(rs));
        },
        userName);
  }

  public static UserDao lookupOrThrow(String userName, TransactionQueryRunner queryRunner) {
    return lookup(userName, queryRunner)
        .orElseThrow(() -> new DatabaseError("User " + userName + " not found."));
  }

  public static ImmutableList<UserDao> lookupAll(UserDao requestingUser, TransactionQueryRunner q) {
    if (requestingUser.isAdmin()) {
      return lookupAllForAdmin(q);
    }
    if (requestingUser.isViewer() || requestingUser.isDelegator()) {
      return lookupAllForViewer(requestingUser, q);
    }
    return ImmutableList.of(requestingUser);
  }

  @AutoValue
  public abstract static class UserPermission {
    public abstract String userName();

    public abstract boolean exists();

    static UserPermission create(String username, boolean exists) {
      return new AutoValue_UserDao_UserPermission(username, exists);
    }
  }

  public static ImmutableSet<UserPermission> lookupAllViewerPermissions(
      UserDao requestingUser, TransactionQueryRunner q) {
    if (requestingUser.isAdmin()) {
      return q.query(
          "select viewedusername, username from "
              + "(select distinct viewedusername from "
              + "(select username as viewedusername from users union "
              + "select viewedusername as username from viewers) as q) as q2 "
              + "left outer join users on viewedusername = username "
              + "order by viewedusername asc",
          rs -> {
            ImmutableSet.Builder<UserPermission> builder = ImmutableSet.builder();
            while (rs.next()) {
              builder.add(
                  UserPermission.create(
                      rs.getString("viewedusername"), rs.getString("username") != null));
            }
            return builder.build();
          });
    }

    if (requestingUser.isDelegator()) {
      return q.query(
          "select viewedusername, users.username from viewers "
              + "left outer join users on viewedusername = users.username "
              + "where viewingusername = ? order by viewedusername",
          rs -> {
            ImmutableSet.Builder<UserPermission> builder = ImmutableSet.builder();
            while (rs.next()) {
              builder.add(
                  UserPermission.create(
                      rs.getString("viewedusername"), rs.getString("username") != null));
            }
            return builder.build();
          },
          requestingUser.userName());
    }
    return ImmutableSet.of();
  }

  /**
   * Lookup all the usernames of users who are viewers of any of the users that I can delegate
   * permissions on.
   */
  public static ImmutableSetMultimap<String, UserPermission> lookupAllViewers(
      UserDao requestingUser, TransactionQueryRunner q) {
    if (requestingUser.isAdmin()) {
      return q.query(
          "select viewingusername,viewedusername, users.username from viewers "
              + "left outer join users on viewedusername = users.username "
              + "order by viewedusername",
          rs -> {
            ImmutableSetMultimap.Builder<String, UserPermission> builder =
                ImmutableSetMultimap.builder();
            while (rs.next()) {
              builder.put(
                  rs.getString("viewingusername"),
                  UserPermission.create(
                      rs.getString("viewedusername"), rs.getString("username") != null));
            }
            return builder.build();
          });
    }
    if (requestingUser.isDelegator()) {
      return q.query(
          "select viewingusername, viewedusername, users.username from viewers "
              + "left outer join users on viewedusername = users.username "
              + "where viewedusername in "
              + "(select viewedusername from viewers where viewingusername = ?) "
              + "order by viewingusername, viewedusername",
          rs -> {
            ImmutableSetMultimap.Builder<String, UserPermission> builder =
                ImmutableSetMultimap.builder();
            while (rs.next()) {
              builder.put(
                  rs.getString("viewingusername"),
                  UserPermission.create(
                      rs.getString("viewedusername"), rs.getString("username") != null));
            }
            return builder.build();
          },
          requestingUser.userName());
    }
    return ImmutableSetMultimap.of();
  }

  public static boolean grantViewPermission(
      UserDao requestingUser,
      String viewingUserName,
      String viewedUserName,
      TransactionQueryRunner q) {
    checkDelegation(requestingUser, viewedUserName, q);
    return q.update(
            "INSERT INTO viewers(viewingusername,viewedusername) values (?,?) "
                + "ON CONFLICT DO NOTHING",
            viewingUserName,
            viewedUserName)
        != 0;
  }

  public static boolean revokeViewPermissions(
      UserDao requestingUser,
      String viewingUserName,
      String viewedUserName,
      TransactionQueryRunner q) {
    checkDelegation(requestingUser, viewedUserName, q);
    return q.update(
            "DELETE FROM viewers WHERE viewingUserName = ? and viewedUsername = ?",
            viewingUserName,
            viewedUserName)
        != 0;
  }

  public static void checkViewerRole(String viewer, TransactionQueryRunner q) {
    boolean canView =
        q.query(
            "SELECT viewingusername from viewers where viewingusername =? Limit 1",
            ResultSet::next,
            viewer);
    if (canView) {
      q.update("UPDATE users set role=1 where username=? and role=0", viewer);
    } else {
      q.update("UPDATE users set role=0 where username=? and role=1", viewer);
    }
  }

  private static ImmutableList<UserDao> lookupAllForAdmin(TransactionQueryRunner q) {
    return q.query(
        "select userid, username, role, fullname, researchoptin from users",
        rs -> {
          ImmutableList.Builder<UserDao> builder = ImmutableList.builder();
          while (rs.next()) {
            builder.add(create(rs));
          }
          return builder.build();
        });
  }

  private static ImmutableList<UserDao> lookupAllForViewer(
      UserDao requestingUser, TransactionQueryRunner q) {
    return q.query(
        "select userid, username, role, fullname, researchoptin from users, viewers where "
            + "viewingusername =? and viewedusername = username",
        rs -> {
          ImmutableList.Builder<UserDao> builder = ImmutableList.builder();
          // you can view yourself
          builder.add(requestingUser);
          while (rs.next()) {
            builder.add(create(rs));
          }
          return builder.build();
        },
        requestingUser.userName());
  }

  private static void checkUserName(String userName) throws DatabaseError {
    if (userName.matches("[^a-z0-9-]")) {
      throw new DatabaseError("Username may only contain lowercase letters, numbers and hyphens");
    }
  }

  private static void checkDelegation(
      UserDao requestingUser, String viewedUserName, TransactionQueryRunner q) {
    boolean permitted =
        requestingUser.isAdmin()
            || (requestingUser.isDelegator()
                && q.query(
                    "SELECT viewedusername from viewers "
                        + "where viewingusername = ? and viewedusername = ?",
                    ResultSet::next,
                    requestingUser.userName(),
                    viewedUserName));
    if (!permitted) {
      throw new AccessDeniedError(
          requestingUser.userName()
              + " does not have permission to delegate access to "
              + viewedUserName);
    }
  }
}
