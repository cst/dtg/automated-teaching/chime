package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import java.util.HashMap;
import java.util.Map;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskAction;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class Submissions {

  public abstract ImmutableList<Action> actions();

  public abstract Action currentAction();

  public static Submissions create(ImmutableList<Action> actions, Action currentAction) {
    return new AutoValue_Submissions(actions, currentAction);
  }

  public static Submissions lookup(
      LocalRepo repo, ImmutableList<TaskAction> taskActions, TransactionQueryRunner q) {
    Map<String, SubmissionDao> latestSubmissions = new HashMap<>();
    Map<String, SubmissionDao> latestPassed = new HashMap<>();
    for (SubmissionDao submissionDao : SubmissionDao.lookup(repo, q)) {
      latestSubmissions.put(submissionDao.action(), submissionDao);
      if (submissionDao.status().equals(SubmissionDao.STATUS_PASSED)) {
        latestPassed.put(submissionDao.action(), submissionDao);
      }
    }
    ImmutableList.Builder<Action> actions = ImmutableList.builder();
    Action currentAction = null;
    Action lastAction = null;
    for (TaskAction taskAction : taskActions) {
      String name = taskAction.name();
      Action.Builder actionBuilder =
          Action.builder().name(name).description(taskAction.description());
      if (latestSubmissions.containsKey(name)) {
        actionBuilder.latestSubmission(latestSubmissions.get(name));
      }
      if (latestPassed.containsKey(name)) {
        actionBuilder.latestPassed(latestPassed.get(name));
      }
      Action action = actionBuilder.build();
      actions.add(action);
      if (currentAction == null && !latestPassed.containsKey(name)) {
        currentAction = action;
      }
      lastAction = action;
    }
    if (currentAction == null) {
      currentAction = lastAction;
    }
    return create(actions.build(), currentAction);
  }
}
