package uk.ac.cam.cl.dtg.teaching.chime.kind;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static uk.ac.cam.cl.dtg.teaching.chime.app.StringUtil.stripNull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Streams;
import java.time.Duration;
import java.util.Date;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.StepDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

public class StatusInOutput implements TaskKind {

  @Override
  public SubmissionDao processSubmission(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      TransactionQueryRunner queryRunner,
      PotteryInterface potteryInterface) {
    return updateSubmissionDao(
        submissionId, localRepo, submissionTime, s, getStepDaos(localRepo, s, potteryInterface));
  }

  static SubmissionDao updateSubmissionDao(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      ImmutableList<StepDao> steps) {
    return SubmissionDao.builder()
        .submissionId(submissionId)
        .localRepo(localRepo)
        .sha1(s.getTag())
        .status(interpretStatus(s.getStatus(), steps))
        .addAllStep(steps)
        .submissionTime(submissionTime)
        .lastUpdateTime(new Date())
        .errorMessage(stripNull(s.getErrorMessage()))
        .action(s.getAction())
        .build();
  }

  static ImmutableList<StepDao> getStepDaos(
      LocalRepo localRepo, Submission s, PotteryInterface potteryInterface) {
    return Streams.mapWithIndex(
            s.getSteps().stream(),
            (step, index) ->
                StepDao.create(
                    step.getName(),
                    step.getStatus(),
                    stripNull(
                        potteryInterface.getStepOutput(
                            localRepo, s.getTag(), s.getAction(), step.getName())),
                    Duration.ofMillis(step.getMsec()),
                    index))
        .collect(toImmutableList());
  }

  static String interpretStatus(String taskStatus, ImmutableList<StepDao> steps) {
    switch (taskStatus) {
      case Submission.STATUS_PENDING:
        return SubmissionDao.STATUS_PENDING;
      case Submission.STATUS_RUNNING:
        return SubmissionDao.STATUS_RUNNING;
      case Submission.STATUS_COMPLETE:
        boolean passed =
            steps.stream()
                .filter(step -> step.name().equals("validator"))
                .findAny()
                .map(StepDao::output)
                .map(String::trim)
                .map(output -> output.endsWith("*** PASSED ALL TESTS ***"))
                .orElse(false);
        return passed ? SubmissionDao.STATUS_PASSED : SubmissionDao.STATUS_FAILED;
    }
    return taskStatus;
  }
}
