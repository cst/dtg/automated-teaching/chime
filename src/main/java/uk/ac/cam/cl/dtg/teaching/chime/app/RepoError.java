package uk.ac.cam.cl.dtg.teaching.chime.app;

public class RepoError extends RuntimeException {

  public RepoError(String message) {
    super(message);
  }
}
