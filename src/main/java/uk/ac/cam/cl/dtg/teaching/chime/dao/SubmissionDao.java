package uk.ac.cam.cl.dtg.teaching.chime.dao;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.Date;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

@AutoValue
public abstract class SubmissionDao {

  public static final String STATUS_NO_SUBMISSION = "NO_SUBMISSION";
  public static final String STATUS_PENDING = Submission.STATUS_PENDING;
  public static final String STATUS_RUNNING = Submission.STATUS_RUNNING;
  public static final String STATUS_PASSED = "PASSED";
  public static final String STATUS_FAILED = "FAILED";

  public abstract int submissionId();

  public abstract LocalRepo localRepo();

  public abstract String sha1();

  public abstract String status();

  public abstract ImmutableList<StepDao> steps();

  public abstract Date submissionTime();

  public abstract Date lastUpdateTime();

  public abstract String errorMessage();

  public abstract String action();

  public void checkCanBeViewedBy(UserDao requestingUser, TransactionQueryRunner q)
      throws AccessDeniedError {
    localRepo().checkCanBeViewedBy(requestingUser, q);
  }

  public void checkCanBeModifiedBy(UserDao requestingUser, TransactionQueryRunner q)
      throws AccessDeniedError {
    localRepo().checkCanBeModifiedBy(requestingUser, q);
  }

  public static Builder builder() {
    return new AutoValue_SubmissionDao.Builder();
  }

  public static SubmissionDao lookupOrThrow(
      UserDao requestingUser, Integer submissionId, TransactionQueryRunner q) {
    SubmissionDao result =
        lookup(submissionId, q).orElseThrow(() -> new DatabaseError("Submission does not exist"));
    result.checkCanBeViewedBy(requestingUser, q);
    return result;
  }

  public static ImmutableList<SubmissionDao> lookup(LocalRepo repo, TransactionQueryRunner q) {
    ImmutableMap<Integer, SubmissionDao.Builder> builders =
        q.query(
            "SELECT submissions.submissionId,"
                + "submissions.sha1,"
                + "submissions.status, "
                + "submissions.submissionTime,"
                + "submissions.lastUpdateTime,"
                + "submissions.errorMessage,"
                + "submissions.action "
                + "from "
                + "submissions where "
                + "submissions.repoid =? "
                + "order by submissions.submissionTime asc",
            rs -> {
              ImmutableMap.Builder<Integer, SubmissionDao.Builder> b = ImmutableMap.builder();
              while (rs.next()) {
                int submissionId = rs.getInt("submissionId");
                b.put(
                    submissionId,
                    SubmissionDao.builder()
                        .localRepo(repo)
                        .submissionId(submissionId)
                        .sha1(rs.getString("sha1"))
                        .status(rs.getString("status"))
                        .submissionTime(new Date(rs.getTimestamp("submissionTime").getTime()))
                        .lastUpdateTime(new Date(rs.getTimestamp("lastUpdateTime").getTime()))
                        .errorMessage(rs.getString("errorMessage"))
                        .action(rs.getString("action")));
              }
              return b.build();
            },
            repo.repoId());

    q.query(
        "SELECT steps.submissionid, "
            + "steps.name, "
            + "steps.status, "
            + "steps.output,"
            + "steps.runtimeMs,"
            + "steps.ordinal from "
            + "submissions, steps where "
            + "steps.submissionid = submissions.submissionid and "
            + "submissions.repoid = ? and "
            + "steps.name !='wait'",
        rs -> {
          while (rs.next()) {
            int submissionId = rs.getInt("submissionId");
            StepDao step =
                StepDao.create(
                    rs.getString("name"),
                    rs.getString("status"),
                    rs.getString("output"),
                    Duration.ofMillis(rs.getLong("runtimeMs")),
                    rs.getLong("ordinal"));
            builders.get(submissionId).addStep(step);
          }
          return null;
        },
        repo.repoId());
    return builders.values().stream().map(SubmissionDao.Builder::build).collect(toImmutableList());
  }

  public static Optional<SubmissionDao> lookup(Integer submissionId, TransactionQueryRunner q) {

    SubmissionDao.Builder builder = SubmissionDao.builder();

    boolean found =
        q.query(
            "SELECT users.userid,"
                + "users.username,"
                + "users.role,"
                + "users.fullname,"
                + "users.researchoptin,"
                + "repos.repoid,"
                + "repos.ownername,"
                + "repos.reponame,"
                + "repos.potterytaskid,"
                + "repos.variant,"
                + "repos.potteryrepoid,"
                + "repos.started,"
                + "submissions.sha1,"
                + "submissions.status, "
                + "submissions.submissionTime,"
                + "submissions.lastUpdateTime,"
                + "submissions.errorMessage,"
                + "submissions.action "
                + "from "
                + "users, repos, submissions where "
                + "users.userid = repos.userid and "
                + "repos.repoid = submissions.repoid and "
                + "submissions.submissionid = ?",
            rs -> {
              if (!rs.next()) {
                return false;
              }
              UserDao userDao =
                  UserDao.create(
                      rs.getString("username"),
                      rs.getInt("userId"),
                      rs.getInt("role"),
                      rs.getString("fullname"),
                      UserDao.ResearchChoice.valueOf(rs.getString("researchoptin")));
              LocalRepo localRepo =
                  LocalRepo.builder()
                      .user(userDao)
                      .repoId(rs.getInt("repoId"))
                      .ownerName(rs.getString("ownername"))
                      .repoName(rs.getString("reponame"))
                      .potteryTaskId(rs.getString("potterytaskid"))
                      .variant(rs.getString("variant"))
                      .potteryRepoId(rs.getString("potteryrepoid"))
                      .startTime(rs.getTimestamp("started").toInstant())
                      .build();
              builder
                  .submissionId(submissionId)
                  .localRepo(localRepo)
                  .sha1(rs.getString("sha1"))
                  .submissionTime(new Date(rs.getTimestamp("submissionTime").getTime()))
                  .lastUpdateTime(new Date(rs.getTimestamp("lastUpdateTime").getTime()))
                  .status(rs.getString("status"))
                  .errorMessage(rs.getString("errorMessage"))
                  .action(rs.getString("action"));
              return true;
            },
            submissionId);

    if (!found) {
      return Optional.empty();
    }

    q.query(
        "SELECT name, status, output, runtimeMs, ordinal from steps where "
            + "name != 'wait' and submissionid = ? order by ordinal",
        rs -> {
          while (rs.next()) {
            builder.addStep(
                StepDao.create(
                    rs.getString("name"),
                    rs.getString("status"),
                    rs.getString("output"),
                    Duration.ofMillis(rs.getLong("runtimeMs")),
                    rs.getLong("ordinal")));
          }
          return null;
        },
        submissionId);

    return Optional.of(builder.build());
  }

  public static Optional<Integer> lookupSubmissionId(
      Integer repoId, String sha1, TransactionQueryRunner q) {
    return q.query(
        "SELECT "
            + "submissions.submissionid FROM submissions where "
            + "submissions.repoid = ? and "
            + "submissions.sha1 = ?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          return Optional.of(rs.getInt("submissionid"));
        },
        repoId,
        sha1);
  }

  public static Optional<SubmissionDao> lookupLatestSubmission(
      Integer repoId, TransactionQueryRunner q) {
    Optional<Integer> submissionId =
        q.query(
            "SELECT "
                + "submissions.submissionid FROM submissions where "
                + "submissions.repoid = ? "
                + "order by submissions.submissionTime desc limit 1",
            rs -> {
              if (!rs.next()) {
                return Optional.empty();
              }
              return Optional.of(rs.getInt("submissionid"));
            },
            repoId);
    return submissionId.flatMap(id -> lookup(id, q));
  }

  public static Optional<SubmissionDao> lookupLatestSubmission(
      Integer repoId, String status, TransactionQueryRunner q) {
    Optional<Integer> submissionId =
        q.query(
            "SELECT "
                + "submissions.submissionid FROM submissions where "
                + "submissions.repoid = ? and "
                + "submissions.status = ? "
                + "order by submissions.submissionTime desc limit 1",
            rs -> {
              if (!rs.next()) {
                return Optional.empty();
              }
              return Optional.of(rs.getInt("submissionid"));
            },
            repoId,
            status);
    return submissionId.flatMap(id -> lookup(id, q));
  }

  public static Optional<SubmissionDao> lookupLatestSubmissionByAction(
      Integer repoId, String action, TransactionQueryRunner q) {
    Optional<Integer> submissionId =
        q.query(
            "SELECT "
                + "submissions.submissionid FROM submissions where "
                + "submissions.repoid = ? AND "
                + "submissions.action = ? "
                + "order by submissions.submissionTime desc limit 1",
            rs -> {
              if (!rs.next()) {
                return Optional.empty();
              }
              return Optional.of(rs.getInt("submissionid"));
            },
            repoId,
            action);
    return submissionId.flatMap(id -> lookup(id, q));
  }

  public static Optional<SubmissionDao> lookupLatestSubmissionByAction(
      Integer repoId, String action, String status, TransactionQueryRunner q) {
    Optional<Integer> submissionId =
        q.query(
            "SELECT "
                + "submissions.submissionid FROM submissions where "
                + "submissions.repoid = ? and "
                + "submissions.status = ? and "
                + "submissions.action = ? "
                + "order by submissions.submissionTime desc limit 1",
            rs -> {
              if (!rs.next()) {
                return Optional.empty();
              }
              return Optional.of(rs.getInt("submissionid"));
            },
            repoId,
            status,
            action);
    return submissionId.flatMap(id -> lookup(id, q));
  }

  public boolean updateWithErrorMessage(String errorMessage, TransactionQueryRunner q) {
    return q.update(
            "UPDATE submissions set errorMessage = ?, status = ? WHERE submissionId = ?",
            errorMessage,
            STATUS_FAILED,
            submissionId())
        != 0;
  }

  public boolean update(TransactionQueryRunner q) {
    int rowsUpdated =
        q.update(
            "UPDATE submissions set "
                + "repoid = ?,"
                + "sha1 = ?, "
                + "status = ?, "
                + "lastUpdateTime = ?,"
                + "errorMessage = ?,"
                + "action = ? where "
                + "submissionid = ?",
            localRepo().repoId(),
            sha1(),
            status(),
            new Timestamp(lastUpdateTime().getTime()),
            errorMessage(),
            action(),
            submissionId());
    if (rowsUpdated == 0) {
      return false;
    }
    for (StepDao step : steps()) {
      int stepsUpdated =
          q.update(
              "UPDATE steps set status=?, output=?, runtimeMs =?, ordinal=? where "
                  + "submissionid=? and name=?",
              step.status(),
              step.output(),
              step.runtime().toMillis(),
              step.ordinal(),
              submissionId(),
              step.name());
      if (stepsUpdated == 0) {
        q.insert(
            "insert into steps(submissionid,name,status,output,runtimeMs,ordinal) "
                + "values (?,?,?,?,?,?)",
            submissionId(),
            step.name(),
            step.status(),
            step.output(),
            step.runtime().toMillis(),
            step.ordinal());
      }
    }
    return true;
  }

  public void insert(TransactionQueryRunner q) {

    q.insert(
        "INSERT INTO submissions("
            + "submissionId, "
            + "repoId, "
            + "sha1, "
            + "submissionTime,"
            + "lastUpdateTime,"
            + "errorMessage,"
            + "action,"
            + "status) values (?,?,?,?,?,?,?,?)",
        submissionId(),
        localRepo().repoId(),
        sha1(),
        new Timestamp(new Date().getTime()),
        new Timestamp(new Date().getTime()),
        errorMessage(),
        action(),
        status());

    for (StepDao step : steps()) {
      q.insert(
          "INSERT INTO steps(submissionId,name,status,output,runtimems,ordinal) "
              + "values (?,?,?,?,?,?)",
          submissionId(),
          step.name(),
          status(),
          step.output(),
          step.runtime().toMillis(),
          step.ordinal());
    }
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder submissionId(int submissionId);

    public abstract int submissionId();

    public abstract Builder localRepo(LocalRepo localRepo);

    public abstract Builder sha1(String sha1);

    public abstract Builder status(String status);

    abstract ImmutableList.Builder<StepDao> stepsBuilder();

    public abstract Builder submissionTime(Date time);

    public abstract Builder lastUpdateTime(Date time);

    public abstract Builder errorMessage(String errorMessage);

    public abstract Builder action(String action);

    public abstract SubmissionDao build();

    public Builder addStep(StepDao step) {
      stepsBuilder().add(step);
      return this;
    }

    public Builder addAllStep(ImmutableList<StepDao> step) {
      stepsBuilder().addAll(step);
      return this;
    }
  }
}
