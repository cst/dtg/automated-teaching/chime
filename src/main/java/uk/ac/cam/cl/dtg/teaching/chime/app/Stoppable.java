package uk.ac.cam.cl.dtg.teaching.chime.app;

public interface Stoppable {

  void stop();
}
