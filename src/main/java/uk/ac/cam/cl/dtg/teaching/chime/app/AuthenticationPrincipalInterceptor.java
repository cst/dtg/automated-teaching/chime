package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import java.security.Principal;
import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.ldap.LDAPObjectNotFoundException;
import uk.ac.cam.cl.dtg.ldap.LDAPQueryManager;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ResearchOptInController;
import uk.ac.cam.cl.dtg.teaching.exceptions.ExceptionHandler;

@Provider
@ServerInterceptor
public class AuthenticationPrincipalInterceptor implements ContainerRequestFilter {

  private static final Logger LOG =
      LoggerFactory.getLogger(AuthenticationPrincipalInterceptor.class);

  static final String SPOOFED_USER_NAME = "SpoofedUserName";

  private static final String TEST_USER_NAME = "test123";

  private final boolean mockRavenAuth;
  private final Database database;
  private final HttpConnectionUri httpConnectionUri;

  @Inject
  public AuthenticationPrincipalInterceptor(
      @Named("mockRavenAuth") boolean mockRavenAuth,
      Database database,
      HttpConnectionUri httpConnectionUri) {
    this.mockRavenAuth = mockRavenAuth;
    this.database = database;
    this.httpConnectionUri = httpConnectionUri;
  }

  private static Optional<String> findPrincipal(ContainerRequestContext requestContext) {

    SecurityContext securityContext = requestContext.getSecurityContext();
    if (securityContext == null) {
      return Optional.empty();
    }

    Principal userPrincipalFromContext = securityContext.getUserPrincipal();
    if (userPrincipalFromContext != null) {
      return Optional.of(userPrincipalFromContext.getName());
    }

    String header = requestContext.getHeaderString("X-AAPrincipal");
    if (header == null) {
      return Optional.empty();
    }
    List<String> fields = Splitter.on(" ").splitToList(header);
    if (fields.size() != 2) {
      LOG.warn("Malformed authentication header: " + header);
      return Optional.empty();
    }

    // TODO(acr31) check that the digest matches expected

    String userName = fields.get(1);

    return Optional.of(userName);
  }

  @Override
  public void filter(ContainerRequestContext requestContext) {
    try {
      Optional<UserDao> user =
          mockRavenAuth
              ? Optional.of(getTestUser(requestContext))
              : findPrincipal(requestContext).map(this::lookupOrCreateUser);

      if (user.isPresent()) {
        LOG.debug("Username  {}", user.get().userName());
        ResteasyProviderFactory.pushContext(UserDao.class, user.get());
        if (user.get().needsResearchChoice()
            && !requestContext.getUriInfo().getPath().equals("/research")) {
          requestContext.abortWith(
              httpConnectionUri.redirectToFrontend(ResearchOptInController.class, ""));
        }
      } else {
        requestContext.abortWith(Response.status(403).build());
      }
    } catch (DatabaseError e) {
      requestContext.abortWith(ExceptionHandler.exceptionToResponse(e, "UNKNOWN"));
    }
  }

  private UserDao getTestUser(ContainerRequestContext requestContext) {
    String userName =
        getUserNameFromRequest(requestContext)
            .orElse(getUserNameFromCookie(requestContext).orElse(TEST_USER_NAME));

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      Optional<UserDao> userDao = UserDao.lookup(userName, q);
      if (!userDao.isPresent()) {
        int role = UserDao.ROLE_NORMAL;
        boolean hasViewingPermissions =
            q.query(
                "SELECT * from viewers where viewingusername=? limit 1", ResultSet::next, userName);
        if (hasViewingPermissions) {
          role = UserDao.ROLE_VIEWER;
        }
        if (userName.startsWith("admin")) {
          role = UserDao.ROLE_ADMIN;
        }
        userDao = Optional.of(UserDao.insert(userName, role, "TestUser " + userName, q));
        q.commit();
      }
      return userDao.get();
    }
  }

  private static String getFullName(String userName) {
    try {
      return LDAPQueryManager.getUser(userName).getDisplayName();
    } catch (LDAPObjectNotFoundException e) {
      return userName;
    }
  }

  private static Optional<String> getUserNameFromCookie(ContainerRequestContext requestContext) {
    return Optional.ofNullable(requestContext.getCookies().get(SPOOFED_USER_NAME))
        .map(Cookie::getValue);
  }

  static Optional<String> getUserNameFromRequest(ContainerRequestContext requestContext) {
    List<String> userNameParams = requestContext.getUriInfo().getQueryParameters().get("user");
    if (userNameParams == null) {
      return Optional.empty();
    }
    String userName = Iterables.getFirst(userNameParams, null);
    if (userName == null) {
      return Optional.empty();
    }
    return Optional.of(userName);
  }

  private UserDao lookupOrCreateUser(String userName) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      Optional<UserDao> userDao = UserDao.lookup(userName, q);
      if (!userDao.isPresent()) {

        int role = UserDao.ROLE_NORMAL;
        boolean hasViewingPermissions =
            q.query(
                "SELECT * from viewers where viewingusername=? limit 1", ResultSet::next, userName);
        if (hasViewingPermissions) {
          role = UserDao.ROLE_VIEWER;
        }
        userDao = Optional.of(UserDao.insert(userName, role, getFullName(userName), q));
        q.commit();
      }
      return userDao.get();
    }
  }
}
