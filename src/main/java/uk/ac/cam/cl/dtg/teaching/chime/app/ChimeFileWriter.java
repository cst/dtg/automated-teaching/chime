package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.protobuf.ByteString;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;

public class ChimeFileWriter {
  private final File repoLocation;
  private final Git git;
  private boolean written = false;

  public ChimeFileWriter(File repoLocation, Git git) {
    this.repoLocation = repoLocation;
    this.git = git;
  }

  public void write(String fileName, ByteString fileContents) {
    File destination = new File(repoLocation, fileName);
    if (destination.exists()) {
      return;
    }
    destination.getParentFile().mkdirs();
    try {
      try (FileOutputStream w = new FileOutputStream(destination)) {
        fileContents.writeTo(w);
      }
      written = true;
      git.add().addFilepattern(fileName).call();
    } catch (IOException | GitAPIException e) {
      throw new GitApiError("Failed to write skeleton file", e);
    }
  }

  public boolean isWritten() {
    return written;
  }
}
