package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class DiffLineDto {

  public enum Operation {
    DELETE,
    INSERT,
    MODIFY,
    CONTEXT,
    OMITTED
  }

  public abstract String original();

  public abstract String replacement();

  public abstract Operation operation();

  public boolean inNewFile() {
    switch (operation()) {
      case INSERT:
      case OMITTED:
      case CONTEXT:
        return true;
      case DELETE:
      case MODIFY:
        return false;
    }
    throw new IllegalArgumentException("Invalid enum value " + operation());
  }

  static DiffLineDto create(String original, String replacement, Operation operation) {
    return new AutoValue_DiffLineDto(original, replacement, operation);
  }
}
