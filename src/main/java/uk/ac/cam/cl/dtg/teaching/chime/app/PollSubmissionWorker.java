package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import java.util.concurrent.atomic.AtomicBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeSubmissionsController;

public class PollSubmissionWorker {

  private static final Logger logger = LoggerFactory.getLogger(PollSubmissionWorker.class);

  private final Database database;
  private final GitLocation gitLocation;
  private final PotteryInterface potteryInterface;
  private final AtomicBoolean isRunning;
  private final AtomicBoolean shouldStop;

  @Inject
  public PollSubmissionWorker(
      Database database, GitLocation gitLocation, PotteryInterface potteryInterface) {
    this.database = database;
    this.gitLocation = gitLocation;
    this.potteryInterface = potteryInterface;
    this.isRunning = new AtomicBoolean(false);
    this.shouldStop = new AtomicBoolean(false);
  }

  public void start() {
    logger.info("Starting submission poller");
    Thread t =
        new Thread(
            () -> {
              try {
                while (!shouldStop.get()) {
                  try (TransactionQueryRunner q = database.getQueryRunner()) {
                    ImmutableList<Integer> pendingSubmissions =
                        q.query(
                            "SELECT submissionId from submissions "
                                + "where status not in ('PASSED','FAILED')",
                            rs -> {
                              ImmutableList.Builder<Integer> results = ImmutableList.builder();
                              while (rs.next()) {
                                results.add(rs.getInt("submissionId"));
                              }
                              return results.build();
                            });

                    for (Integer submissionId : pendingSubmissions) {
                      SubmissionDao.lookup(submissionId, q).ifPresent(s -> processSubmission(q, s));
                      q.commit();
                    }
                  } catch (Throwable throwable) {
                    logger.error("Caught exception in polling loop", throwable);
                  }
                  try {
                    Thread.sleep(10);
                  } catch (InterruptedException e) {
                    // fall through
                  }
                }
              } finally {
                isRunning.set(false);
              }
            });
    t.setDaemon(true);
    t.start();
    isRunning.set(true);
  }

  private void processSubmission(TransactionQueryRunner q, SubmissionDao submission) {
    try {
      ChimeSubmissionsController.updateSubmission(submission, q, gitLocation, potteryInterface);
    } catch (PotteryError e) {
      try {
        submission.updateWithErrorMessage(e.toString(), q);
      } catch (DatabaseError e2) {
        logger.error("Failed to record error message for " + submission.submissionId(), e2);
      }
      logger.error(
          "Caught exception in polling loop for submission " + submission.submissionId(), e);
    }
  }

  public void stop() {
    shouldStop.set(true);
  }
}
