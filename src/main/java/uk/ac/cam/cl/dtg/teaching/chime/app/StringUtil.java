package uk.ac.cam.cl.dtg.teaching.chime.app;

public class StringUtil {

  public static String fileFriendly(String name) {
    return name.replace("*", "star")
        .replaceAll("[^ a-zA-Z0-9-]", "")
        .replace(" ", "_")
        .toLowerCase();
  }

  public static String stripNull(String v) {
    if (v == null) {
      return v;
    }
    return v.replace('\0', '?');
  }
}
