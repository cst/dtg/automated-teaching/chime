package uk.ac.cam.cl.dtg.teaching.chime.ssh;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.sshd.git.GitLocationResolver;
import org.apache.sshd.server.session.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

class ChimeGitLocationResolver implements GitLocationResolver {

  private static final Logger logger = LoggerFactory.getLogger(ChimePublicKeyAuthenticator.class);

  private final Path gitRepoRootDir;
  private final Database database;

  ChimeGitLocationResolver(String gitRepoRootDir, Database database) {
    this.gitRepoRootDir = Paths.get(gitRepoRootDir);
    this.database = database;
  }

  @Override
  public Path resolveRootDirectory(
      String command, String[] args, ServerSession session, FileSystem fs) throws IOException {

    String userName = session.getUsername();

    if (userName.equals(SshDaemon.POTTERY_USER)) {
      return gitRepoRootDir;
    }

    String repositoryName = args[args.length - 1].replaceAll("^/", "");
    String[] repositoryParts = repositoryName.split("/");

    logger.info("Resolving root for {} on {} with command {}", userName, repositoryName, command);

    if (repositoryParts.length != 2) {
      throw new IOException("Cannot parse repository name " + repositoryName);
    }

    String ownerName = repositoryParts[0];
    String repoName = repositoryParts[1];

    try (TransactionQueryRunner q = database.getQueryRunner()) {

      UserDao requestingUser =
          UserDao.lookup(userName, q).orElseThrow(() -> new IOException("User not found"));
      LocalRepo localRepo =
          LocalRepo.lookup(ownerName, repoName, q)
              .orElseThrow(() -> new IOException(repositoryName + " not found"));

      boolean canWrite = localRepo.user().canBeModifiedBy(requestingUser, q);
      boolean canView = canWrite || localRepo.user().canBeViewedBy(requestingUser, q);
      boolean writeRequest = command.contains("receive-pack");

      if (canWrite || (canView && !writeRequest)) {
        logger.info(
            "Resolved {} for user {} accessing user {}, writeable: {}",
            gitRepoRootDir,
            requestingUser.userName(),
            localRepo.user().userName(),
            canWrite);
        return gitRepoRootDir;
      }

    } catch (DatabaseError e) {
      throw new IOException(e);
    }

    throw new IOException("Permission denied");
  }
}
