package uk.ac.cam.cl.dtg.teaching.chime.app;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;

@SuppressWarnings("serial")
@WebServlet(
    urlPatterns = {"/page/*"},
    initParams = {@WebInitParam(name = "resteasy.servlet.mapping.prefix", value = "/page/")})
public class HttpServletDispatcherV3 extends HttpServletDispatcher {}
