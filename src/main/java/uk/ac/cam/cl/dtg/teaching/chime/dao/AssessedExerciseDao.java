package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.util.Date;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class AssessedExerciseDao {

  public abstract String potteryTaskId();

  public abstract Date deadline();

  public abstract boolean online();

  public static AssessedExerciseDao create(String potteryTaskId, Date deadline, boolean online) {
    return new AutoValue_AssessedExerciseDao(potteryTaskId, deadline, online);
  }

  public static Optional<AssessedExerciseDao> lookup(
      String potteryTaskId, TransactionQueryRunner queryRunner) {
    return queryRunner.query(
        "SELECT deadline, online from assessedexercise where potteryTaskId = ?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          return Optional.of(
              create(
                  potteryTaskId,
                  new Date(rs.getTimestamp("deadline").getTime()),
                  rs.getBoolean("online")));
        },
        potteryTaskId);
  }
}
