package uk.ac.cam.cl.dtg.teaching.chime.database;

import uk.ac.cam.cl.dtg.teaching.chime.app.Stoppable;

public interface Database extends Stoppable {

  TransactionQueryRunner getQueryRunner();

  @Override
  void stop();
}
