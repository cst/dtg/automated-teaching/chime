package uk.ac.cam.cl.dtg.teaching.chime.database;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class TransactionQueryRunner implements Closeable {

  private static final Logger LOG = LoggerFactory.getLogger(TransactionQueryRunner.class);

  private final Connection connection;
  private final QueryRunner queryRunner;

  public TransactionQueryRunner(DataSource datasource) {
    try {
      connection = datasource.getConnection();
      connection.setAutoCommit(false);
      queryRunner = new QueryRunner();
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }

  public void commit() {
    try {
      connection.commit();
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }

  public void rollback() {
    try {
      connection.rollback();
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }

  public void close() {
    try {
      connection.close();
    } catch (SQLException e) {
      LOG.warn("Exception (suppressed) closing database connection", e);
    }
  }

  public abstract int nextVal(String sequence);

  public <T> T query(String sql, ResultSetHandler<T> rsh, Object... params) {
    try {
      return queryRunner.query(connection, sql, rsh, params);
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }

  public <T> T query(String sql, ResultSetHandler<T> rsh) {
    return query(sql, rsh, (Object[]) null);
  }

  public int update(String sql) {
    return update(sql, (Object[]) null);
  }

  public int update(String sql, Object... params) {
    try {
      return queryRunner.update(connection, sql, params);
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }

  public void insert(String sql) {
    insert(sql, (Object[]) null);
  }

  public void insert(String sql, Object... params) {
    try {
      queryRunner.insert(connection, sql, (ResultSetHandler<Void>) rs -> null, params);
    } catch (SQLException e) {
      throw new DatabaseError(e);
    }
  }
}
