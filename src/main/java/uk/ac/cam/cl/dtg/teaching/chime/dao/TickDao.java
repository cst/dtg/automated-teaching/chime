package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;
import javax.xml.bind.DatatypeConverter;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class TickDao {

  public static final String NO_ASSESSOR = "N/A";

  public enum TickState {
    NOT_ASSESSED,
    NO_SHOW,
    CORRECTION_NEEDED,
    AWARDED
  }

  public abstract LocalRepo localRepo();

  public abstract Date deadline();

  public abstract TickState tickState();

  public abstract String sha1();

  public abstract String assessedBy();

  public abstract boolean online();

  public String meetingUrl(String meetingGenerationNonce) {
    try {
      MessageDigest md = MessageDigest.getInstance("MD5");
      md.update(
          (localRepo().user().userName() + meetingGenerationNonce)
              .getBytes(StandardCharsets.UTF_8));
      byte[] digest = md.digest();
      String hash = DatatypeConverter.printHexBinary(digest).toUpperCase();

      return String.format(
          "https://accounts.google.com/ServiceLogin?ltmpl=meet&continue=https://g.co/meet/%s&passive=true&hd=cam.ac.uk",
          hash);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException(e);
    }
  }

  public boolean deadlinePassed() {
    return deadline().before(new Date());
  }

  public static Builder builder() {
    return new AutoValue_TickDao.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder localRepo(LocalRepo localRepo);

    public abstract Builder deadline(Date deadline);

    public abstract Builder tickState(TickState tickState);

    public abstract Builder assessedBy(String assessedBy);

    public abstract Builder sha1(String sha1);

    public abstract TickDao build();

    public abstract Builder online(boolean online);
  }

  public static Optional<TickDao> lookup(LocalRepo localRepo, TransactionQueryRunner q) {
    return q.query(
        "SELECT deadline, tickstate, assessedby,sha1, online from ticks where repoid =?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          return Optional.of(
              TickDao.builder()
                  .localRepo(localRepo)
                  .deadline(new Date(rs.getTimestamp("deadline").getTime()))
                  .tickState(TickState.valueOf(rs.getString("tickState")))
                  .assessedBy(rs.getString("assessedby"))
                  .sha1(rs.getString("sha1"))
                  .online(rs.getBoolean("online"))
                  .build());
        },
        localRepo.repoId());
  }

  public void insert(TransactionQueryRunner q) {
    q.insert(
        "INSERT INTO ticks(repoid,deadline,tickState,assessedby,sha1, online) VALUES (?,?,?,?,?,?)",
        localRepo().repoId(),
        new Timestamp(deadline().getTime()),
        tickState().name(),
        assessedBy(),
        sha1(),
        online());
  }

  public void setDeadline(Date deadline, TransactionQueryRunner q) {
    q.update(
        "UPDATE ticks set deadline=? where repoid =?",
        new Timestamp(deadline.getTime()),
        localRepo().repoId());
  }

  public void setTickState(
      UserDao user, TickState tickState, String sha1, TransactionQueryRunner q) {
    q.update(
        "UPDATE ticks set tickState=?, sha1=?, assessedby=? where repoid =?",
        tickState.name(),
        sha1,
        user.userName(),
        localRepo().repoId());
  }
}
