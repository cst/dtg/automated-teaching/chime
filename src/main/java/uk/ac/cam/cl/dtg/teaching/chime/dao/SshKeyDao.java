package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import org.apache.sshd.common.config.keys.PublicKeyEntry;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class SshKeyDao {

  public abstract int keyId();

  public abstract UserDao user();

  public abstract String key();

  public PublicKey toPublicKey() throws GeneralSecurityException {
    try {
      PublicKeyEntry publicKeyEntry = PublicKeyEntry.parsePublicKeyEntry(key());
      return publicKeyEntry.resolvePublicKey(null, null, null);
    } catch (IllegalArgumentException | IOException e) {
      throw new GeneralSecurityException(e);
    }
  }

  public boolean canParse() {
    try {
      toPublicKey();
      return true;
    } catch (GeneralSecurityException e) {
      return false;
    }
  }

  private static SshKeyDao create(int keyId, UserDao user, String key) {
    return new AutoValue_SshKeyDao(keyId, user, key);
  }

  public static ImmutableList<SshKeyDao> lookupKeys(UserDao user, TransactionQueryRunner q) {
    return q.query(
        "SELECT keyId, key from sshkeys where userid = ?",
        rs -> {
          ImmutableList.Builder<SshKeyDao> result = ImmutableList.builder();
          while (rs.next()) {
            result.add(create(rs.getInt("keyId"), user, rs.getString("key")));
          }
          return result.build();
        },
        user.userId());
  }

  public static SshKeyDao createNew(UserDao user, String key, TransactionQueryRunner q) {
    int keyId = q.nextVal("sshKeyIds");
    q.insert("insert into sshkeys(keyid, userid, key) values (?,?,?)", keyId, user.userId(), key);
    return create(keyId, user, key);
  }

  public static boolean delete(UserDao user, Integer keyId, TransactionQueryRunner q) {
    return q.update("DELETE from sshkeys WHERE userid = ? and keyid = ?", user.userId(), keyId)
        != 0;
  }
}
