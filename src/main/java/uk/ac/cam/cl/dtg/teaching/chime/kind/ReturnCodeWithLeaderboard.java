package uk.ac.cam.cl.dtg.teaching.chime.kind;

import java.sql.Timestamp;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.StepDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

public class ReturnCodeWithLeaderboard extends ReturnCodeOnly {

  public static final Pattern LEADERBOARD_SCORE =
      Pattern.compile(".*\\nScore: (.*) \\(ordering ([0-9]+)\\)");

  @Override
  public SubmissionDao processSubmission(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      TransactionQueryRunner queryRunner,
      PotteryInterface potteryInterface) {

    SubmissionDao result =
        super.processSubmission(
            submissionId, localRepo, submissionTime, s, queryRunner, potteryInterface);

    if (result.status().equals(SubmissionDao.STATUS_PASSED)) {
      for (StepDao step : result.steps()) {
        Matcher m = LEADERBOARD_SCORE.matcher(step.output());
        if (m.find()) {
          String score = m.group(1);
          int order = Integer.parseInt(m.group(2));
          if (score.length() > 30) {
            continue;
          }
          boolean foundRow =
              queryRunner.update(
                      "UPDATE leaderboard set ordering = ?, "
                          + "timerecorded = ?,"
                          + "score = ? "
                          + "where userid = ? and potteryTaskId = ?",
                      order,
                      new Timestamp(submissionTime.getTime()),
                      score,
                      localRepo.user().userId(),
                      localRepo.potteryTaskId())
                  != 0;
          if (!foundRow) {
            queryRunner.insert(
                "INSERT into leaderboard(userid,"
                    + "ordering,"
                    + "timerecorded,"
                    + "score,"
                    + "potterytaskid) values (?,?,?,?,?)",
                localRepo.user().userId(),
                order,
                new Timestamp(submissionTime.getTime()),
                score,
                localRepo.potteryTaskId());
          }
        }
      }
    }

    return result;
  }
}
