package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@Produces("text/html")
@Path("/research")
public class ResearchOptInController {

  private final TemplateProcessor templateProcessor;
  private final Database database;
  private final Messages messages;
  private final HttpConnectionUri httpConnectionUri;

  @Inject
  public ResearchOptInController(
      TemplateProcessor templateProcessor,
      Database database,
      Messages messages,
      HttpConnectionUri httpConnectionUri) {
    this.templateProcessor = templateProcessor;
    this.database = database;
    this.messages = messages;
    this.httpConnectionUri = httpConnectionUri;
  }

  @GET
  @Path("/")
  public String show(@Context UserDao user) {
    return templateProcessor.process("research_opt_in.ftl", user, ImmutableMap.of());
  }

  @POST
  @Path("/")
  public Response update(@Context UserDao user, @FormParam("choice") String choice) {
    if (choice == null) {
      return httpConnectionUri.redirectToFrontend(ResearchOptInController.class, "");
    }
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      user.updateResearchOptIn(UserDao.ResearchChoice.valueOf(choice), q);
      q.commit();
    }
    messages.postMessage(user, "Your research preferences have been updated");
    return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, "");
  }
}
