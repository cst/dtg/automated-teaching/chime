package uk.ac.cam.cl.dtg.teaching.chime.app;

public class TaskError extends RuntimeException {

  public TaskError(String message) {
    super(message);
  }
}
