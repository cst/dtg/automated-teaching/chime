package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import org.junit.Before;
import org.junit.Test;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateConfiguration;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.NewLocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.StepDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.InMemoryDatabase;
import uk.ac.cam.cl.dtg.teaching.pottery.api.mock.PotteryBackendMock;

public class ChimeSubmissionsControllerTest {

  private ChimeSubmissionsController submissionsController;
  private InMemoryDatabase database;

  @Before
  public void setup() {
    database = new InMemoryDatabase();
    Messages messages = new Messages();
    HttpConnectionUri httpConnectionUri = new HttpConnectionUri("http://test.com/", "/api/");
    GitLocation gitLocation = new GitLocation("test.com", 222, "/git/root/dir");
    TemplateProcessor templateProcessor =
        new TemplateProcessor(
            new TemplateConfiguration(),
            "http://test.com/",
            false,
            "",
            messages,
            httpConnectionUri,
            gitLocation);
    PotteryInterface potteryInterface = new PotteryInterface(new PotteryBackendMock(), false);
    submissionsController =
        new ChimeSubmissionsController(
            database, templateProcessor, potteryInterface, httpConnectionUri, gitLocation);
  }

  @Test
  public void showSubmission_doesntThrow() {
    // ARRANGE
    UserDao requestingUser =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "ADMIN FULL NAME", q));
    LocalRepo localRepo =
        database.execute(
            q ->
                NewLocalRepo.builder()
                    .ownerName(requestingUser.userName())
                    .potteryRepoId("pottery-repo-id")
                    .potteryTaskId("pottery-task-id")
                    .repoName("test-repo-name")
                    .user(requestingUser)
                    .variant("test-variant")
                    .startTime(Instant.now())
                    .build()
                    .insert(q));
    int submissionId =
        database.execute(
            q -> {
              int newSubmissionId = q.nextVal("submissionIds");
              SubmissionDao.builder()
                  .submissionId(newSubmissionId)
                  .localRepo(localRepo)
                  .sha1("test-sha1")
                  .status(SubmissionDao.STATUS_PASSED)
                  .addStep(
                      StepDao.create(
                          "test-step", "step-status", "STEP OUTPUT", Duration.ofMinutes(5), 0))
                  .submissionTime(new Date())
                  .lastUpdateTime(new Date())
                  .errorMessage("")
                  .action("test-action")
                  .build()
                  .insert(q);
              return newSubmissionId;
            });

    // ACT
    submissionsController.showSubmission(requestingUser, submissionId);
  }
}
